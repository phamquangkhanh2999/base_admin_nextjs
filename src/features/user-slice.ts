import { RootState, store } from '@/app/store';
import {
  IChangePasswordPayload,
  IConfirmOtpPayload,
  IForgotPasswordPayload,
  IInitState,
  ILoginResponse,
  ILogoutPayload,
  IPayloadLogin,
  IPayloadRefresToken,
  IRegisterResponse,
  IResetPasswordPayload,
  IResponseGetListUser,
  IUpdateProfilePayload,
  IUserSendOtpPayload,
  IUserSendOtpResponse,
  IVerifyAccountPayload,
} from '@/models/user';
import { createSlice, createAsyncThunk, createAction } from '@reduxjs/toolkit';
import userApi from './services/user-api';

export const USER_SLICE_NAME: string = 'user';
export const USER_SIGN_IN: string = 'auth/signIn';
export const REFRESH_TOKEN: string = 'user/refresh-token';
export const USER_CHANGE_PASSWORD: string = 'user/change-password';
export const LOGOUT: string = 'user/logout';
export const GET_USER_BY_ID: string = 'get_user_by_id';
export const UPDATE_PROFILE_BYID: string = 'update_user_by_id';
export const UPDATE_IMAGES: string = 'update_images';
export const USER_GET_LIST = 'user_get_list';
export const USER_BY_ID = 'user_by_id_user';
export const USER_FORGOT_PASSWORD: string = 'user/forgot-password';
export const SEND_OTP: string = 'user/send-otp';
export const VERIFY_OTP: string = 'user/confirm-otp';
export const VERIFY_ACCOUNT: string = 'user/verify-account';
export const RESET_PASSWORD: string = 'user/reset-password';

export const userSignIn = createAsyncThunk(
  USER_SIGN_IN,
  async (payload: IPayloadLogin, { rejectWithValue }) => {
    try {
      const response = await userApi.userSignIn(payload);
      return response.data;
    } catch (err: any) {
      if (!err.response) {
        throw err;
      }
      throw rejectWithValue(err.response.data);
    }
  }
);

export const userRefreshToken = createAsyncThunk(
  REFRESH_TOKEN,
  async (payload: IPayloadRefresToken, { rejectWithValue }) => {
    try {
      const response = await userApi.userRefreshToken(payload);
      return response.data;
    } catch (err: any) {
      if (!err.response) {
        throw err;
      }
      return rejectWithValue(err.response.data);
    }
  }
);

export const userChangePassword = createAsyncThunk(
  USER_CHANGE_PASSWORD,
  async (payload: IChangePasswordPayload, { rejectWithValue }) => {
    try {
      const response = await userApi.userChangePassword(payload);
      return response.data;
    } catch (err: any) {
      if (!err.response) {
        throw err.response;
      }
      return rejectWithValue(err.response.data);
    }
  }
);

export const userLogout = createAsyncThunk(
  LOGOUT,
  async (payload: ILogoutPayload, { rejectWithValue }) => {
    try {
      const response = await userApi.userLogout(payload);
      return response.data;
    } catch (err: any) {
      if (!err.response) {
        throw err.response;
      }
      return rejectWithValue(err.response.data);
    }
  }
);

export const getUserbyID = createAsyncThunk(GET_USER_BY_ID, async () => {
  try {
    const response = await userApi.getUserByID();
    return response.data;
  } catch (err: any) {
    if (!err.response) {
      throw err.response;
    }
  }
});

export const updateProfileById = createAsyncThunk(
  UPDATE_PROFILE_BYID,
  async (payload: IUpdateProfilePayload, { rejectWithValue }) => {
    try {
      const response = await userApi.updateProfile(payload);
      return response.data;
    } catch (err: any) {
      if (!err.response) {
        throw err.response;
      }
      return rejectWithValue(err.response.data);
    }
  }
);
export const createUpdateImages = createAsyncThunk(
  UPDATE_IMAGES,
  async (payload: any, { rejectWithValue }) => {
    try {
      const response = await userApi.updateImages(payload);
      return response.data;
    } catch (err: any) {
      if (!err.response) {
        throw err.response;
      }
      return rejectWithValue(err.response.data);
    }
  }
);
export const getListUser = createAsyncThunk(
  USER_GET_LIST,
  async (payload: any, { rejectWithValue }) => {
    try {
      const response = await userApi.getAllUser(payload);
      return response.data;
    } catch (err: any) {
      if (!err.response) {
        throw err.response;
      }
      return rejectWithValue(err.response.data);
    }
  }
);
export const getByIdUser = createAsyncThunk(
  USER_BY_ID,
  async (payload: any, { rejectWithValue }) => {
    try {
      const response = await userApi.getByIdUser(payload);
      return response.data;
    } catch (err: any) {
      if (!err.response) {
        throw err.response;
      }
      return rejectWithValue(err.response.data);
    }
  }
);
export const userForgotPassword = createAsyncThunk(
  USER_FORGOT_PASSWORD,
  async (payload: IForgotPasswordPayload, { rejectWithValue }) => {
    try {
      const response = await userApi.userForgotPassword(payload);
      return response.data;
    } catch (err: any) {
      if (!err.response) {
        throw err.response;
      }
      return rejectWithValue(err.response.data);
    }
  }
);
export const userSendOtp = createAsyncThunk(
  SEND_OTP,
  async function (payload: IUserSendOtpPayload, { rejectWithValue }) {
    try {
      const response = await userApi.userSendOtp(payload);
      return response.data;
    } catch (err: any) {
      if (!err.response) {
        throw err;
      }
      return rejectWithValue(err.response.data);
    }
  }
);
export const userConfirmOtp = createAsyncThunk(
  VERIFY_OTP,
  async (payload: IConfirmOtpPayload, { rejectWithValue }) => {
    try {
      const response = await userApi.userConfirmOtp(payload);
      return response.data;
    } catch (err: any) {
      if (!err.response) {
        throw err;
      }
      return rejectWithValue(err.response.data);
    }
  }
);
export const userVerifyAccount = createAsyncThunk(
  VERIFY_ACCOUNT,
  async (payload: IVerifyAccountPayload, { rejectWithValue }) => {
    try {
      const response = await userApi.userVerifyAccount(payload);
      return response.data;
    } catch (err: any) {
      if (!err.response) {
        throw err.response;
      }
      return rejectWithValue(err.response.data);
    }
  }
);
export const userResetPassword = createAsyncThunk(
  RESET_PASSWORD,
  async (payload: IResetPasswordPayload, { rejectWithValue }) => {
    try {
      const response = await userApi.userResetPassword(payload);
      return response.data;
    } catch (err: any) {
      if (!err.response) {
        throw err.response;
      }
      return rejectWithValue(err.response.data);
    }
  }
);
const storage = typeof window !== 'undefined' ? localStorage.getItem('wb_a') : undefined;

const initialState: IInitState = {
  loadingGetListUser: false,
  loadingSendOtp: false,
  token: {},
  otp: {},
  isForgotPassWord: false,
  listDataUsers: {} as IResponseGetListUser,
  sendOtp: {} as IUserSendOtpResponse,
  register: {} as IRegisterResponse,
  loading: false,
  error: false,
  isAuthentication: storage ? true : false,
  loginInfo: storage ? (JSON.parse(storage) as ILoginResponse) : ({} as ILoginResponse),
};

const userSlice = createSlice({
  name: USER_SLICE_NAME,
  initialState,
  reducers: {},
  extraReducers(builder) {
    builder
      .addCase(userSignIn.pending, (state) => {
        state.loading = true;
      })
      .addCase(userSignIn.fulfilled, (state, { payload }) => {
        state.loading = false;
        state.isAuthentication = true;
        state.loginInfo = payload as ILoginResponse;
        if (typeof window !== 'undefined') {
          localStorage.setItem('wb_a', JSON.stringify(payload));
        }
      })
      .addCase(userSignIn.rejected, (state) => {
        state.loading = false;
        state.error = true;
      })
      .addCase(userChangePassword.pending, (state) => {
        state.loading = true;
      })
      .addCase(userChangePassword.fulfilled, (state, { payload }) => {
        state.loading = false;
      })
      .addCase(userChangePassword.rejected, (state, action) => {
        state.loading = false;
        state.error = true;
      })
      .addCase(userLogout.pending, (state) => {
        state.loading = true;
      })
      .addCase(userLogout.fulfilled, (state, { payload }) => {
        state.loading = false;
        state.isAuthentication = false;
        if (typeof window !== 'undefined') {
          localStorage.clear();
        }
      })
      .addCase(userLogout.rejected, (state, action) => {
        state.loading = false;
        state.error = true;
      })
      .addCase(userRefreshToken.pending, (state) => {
        state.loading = true;
      })
      .addCase(userRefreshToken.fulfilled, (state, { payload }) => {
        if (typeof window !== 'undefined') {
          localStorage.setItem('wb_a', JSON.stringify(payload));
        }
        state.loading = false;
        state.loginInfo = payload;
      })
      .addCase(userRefreshToken.rejected, (state, { payload }) => {
        state.loading = false;
        state.error = true;
      })
      .addCase(getUserbyID.pending, (state) => {
        state.loading = true;
      })
      .addCase(getUserbyID.fulfilled, (state, { payload }) => {
        state.loading = false;
      })
      .addCase(getUserbyID.rejected, (state, action) => {
        state.loading = false;
        state.error = true;
      })
      .addCase(updateProfileById.pending, (state) => {
        state.loading = true;
      })
      .addCase(updateProfileById.fulfilled, (state, { payload }) => {
        state.loading = false;
      })
      .addCase(updateProfileById.rejected, (state, action) => {
        state.loading = false;
        state.error = true;
      })
      .addCase(createUpdateImages.pending, (state) => {
        state.loading = true;
      })
      .addCase(createUpdateImages.fulfilled, (state, { payload }) => {
        state.loading = false;
      })
      .addCase(createUpdateImages.rejected, (state, action) => {
        state.loading = false;
        state.error = true;
      })
      .addCase(getListUser.pending, (state) => {
        state.loadingGetListUser = true;
      })
      .addCase(getListUser.fulfilled, (state, { payload }) => {
        state.listDataUsers = payload;
        state.loadingGetListUser = false;
      })
      .addCase(getListUser.rejected, (state, action) => {
        state.loadingGetListUser = false;
        state.error = true;
      })
      .addCase(getByIdUser.pending, (state) => {
        state.loading = true;
      })
      .addCase(getByIdUser.fulfilled, (state, { payload }) => {
        state.loading = false;
      })
      .addCase(getByIdUser.rejected, (state, action) => {
        state.loading = false;
        state.error = true;
      })
      /* Send OTP */
      .addCase(userSendOtp.pending, (state) => {
        state.loadingSendOtp = true;
        state.loading = true;
      })
      .addCase(userSendOtp.fulfilled, (state, { payload }) => {
        // When the API call is successful and we get some data,the data becomes the `fulfilled` action payload
        state.sendOtp = payload as IUserSendOtpResponse;
        // state.isAuthentication = true;
        state.loadingSendOtp = false;
        state.loading = false;
      })
      .addCase(userSendOtp.rejected, (state, action) => {
        state.loadingSendOtp = false;
        state.error = true;
        state.loading = false;
      })
      /* Forgot Password */
      .addCase(userForgotPassword.pending, (state) => {
        state.loading = true;
      })
      .addCase(userForgotPassword.fulfilled, (state, { payload }) => {
        state.isAuthentication = false;
        state.loading = false;
        state.isForgotPassWord = true;
        state.token = payload;

        // state.token=payload.
      })
      .addCase(userForgotPassword.rejected, (state, action) => {
        state.loading = false;
        state.error = true;
      })
      /* Confirm OTP */
      .addCase(userConfirmOtp.pending, (state) => {
        // state.loading = true;
      })
      .addCase(userConfirmOtp.fulfilled, (state, { payload }) => {
        // When the API call is successful and we get some data,the data becomes the `fulfilled` action payload
        state.otp = payload as IUserSendOtpResponse;
        // state.isAuthentication = true;
        state.loading = false;
      })
      .addCase(userConfirmOtp.rejected, (state, action) => {
        state.loading = false;
        state.error = true;
      })
      .addCase(userVerifyAccount.pending, (state) => {
        state.loading = true;
      })
      .addCase(userVerifyAccount.fulfilled, (state, { payload }) => {
        // When the API call is successful and we get some data,the data becomes the `fulfilled` action payload
        state.register = payload as IRegisterResponse;
        // state.isAuthentication = true;
        state.loading = false;
      })
      .addCase(userVerifyAccount.rejected, (state, { payload }) => {
        state.loading = false;
        state.error = true;
      })
      /* Reset Password */
      .addCase(userResetPassword.pending, (state) => {
        state.loading = false;
      })
      .addCase(userResetPassword.fulfilled, (state, { payload }) => {
        state.isAuthentication = false;
        state.loading = false;
        state.isForgotPassWord = false;
        // state.token=payload.
      })
      .addCase(userResetPassword.rejected, (state, action) => {
        state.loading = false;
        state.error = true;
      });
  },
});

const { reducer, actions } = userSlice;
export const selectUser = (state: RootState) => state.user;
export default reducer;
