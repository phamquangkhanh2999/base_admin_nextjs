import {
  IChangePasswordPayload,
  IConfirmOtpPayload,
  IForgotPasswordPayload,
  ILogoutPayload,
  IPayloadLogin,
  IPayloadRefresToken,
  IResetPasswordPayload,
  IUpdateProfilePayload,
  IUserSendOtpPayload,
  IVerifyAccountPayload,
} from '@/models/user';
import axiosClient from './axios-client';

class UserApi {
  ///SignIn
  userSignIn(payload: IPayloadLogin) {
    return axiosClient({
      method: 'post',
      url: '/auth/login',
      data: payload,
    });
  }
  userRefreshToken(payload: IPayloadRefresToken) {
    return axiosClient({
      method: 'post',
      url: '/msa-identity/odata/Auth/RefreshToken',
      data: payload,
    });
  }
  userChangePassword(payload: IChangePasswordPayload) {
    return axiosClient({
      method: 'post',
      url: '/msa-identity/odata/Me/ChangePassword',
      data: payload,
    });
  }
  userLogout(payload: ILogoutPayload) {
    return axiosClient({
      method: 'post',
      url: '/msa-identity/odata/Auth/RevokeToken',
      data: payload,
    });
  }
  getUserByID() {
    return axiosClient({
      method: 'get',
      url: `msa-user-profile/odata/Me/Primary()`,
    });
  }
  updateProfile(payload: IUpdateProfilePayload) {
    return axiosClient({
      method: 'put',
      url: `/msa-user-profile/api/Me/${payload.Id}`,
      data: payload,
    });
  }
  updateImages(payload: any) {
    return axiosClient({
      method: 'post',
      url: `/msa-storage/odata/storage/upload`,
      data: payload,
    });
  }
  getAllUser(payload: any) {
    return axiosClient({
      method: 'get',
      url: `msa-identity/api/Users?$count=true`,
      params: payload,
    });
  }
  getByIdUser(payload: any) {
    return axiosClient({
      method: 'get',
      url: `msa-identity/api/Users/${payload?.Id}`,
    });
  }
  userForgotPassword(payload: IForgotPasswordPayload) {
    return axiosClient({
      method: 'post',
      url: '/msa-identity/api/Auth/ForgotPassword',
      data: payload,
    });
  }
  userSendOtp(payload: IUserSendOtpPayload) {
    return axiosClient({
      method: 'post',
      url: '/msa-notification/odata/Otp/SendOtp',
      data: payload,
    });
  }
  userConfirmOtp(payload: IConfirmOtpPayload) {
    return axiosClient({
      method: 'post',
      url: '/msa-notification/odata/Otp/VerifyOtp',
      data: payload,
    });
  }
  userVerifyAccount(payload: IVerifyAccountPayload) {
    return axiosClient({
      method: 'post',
      url: '/msa-identity/odata/Auth/VerifyAccount',
      data: payload,
    });
  }
  userResetPassword(payload: IResetPasswordPayload) {
    return axiosClient({
      method: 'post',
      url: '/msa-identity/odata/Auth/ResetPassword',
      data: payload,
    });
  }
}

export default new UserApi();
