import Head from 'next/head';
import React from 'react';
import BreadcrumbComponent from '@/components/breadcrumb-component/breadcrumb-component';
import { MainLayoutHome } from '@/components/layouts/main-layout/main-layout';
import { IBreadcrumb } from '@/models/common';

const dataHeadTile = { title: 'Học phí toàn trường' };
const dataBreadcrumb: IBreadcrumb[] = [];
const SchoolWideTuitionPage = () => {
  return (
    <React.Fragment>
      <Head>
        <title>{dataHeadTile.title}</title>
        <meta name="description" content={dataHeadTile.title} />
      </Head>
      <BreadcrumbComponent data={dataBreadcrumb} />
      <h1>Học phí toàn trường</h1>
    </React.Fragment>
  );
};
SchoolWideTuitionPage.Layout = MainLayoutHome;
export default SchoolWideTuitionPage;
