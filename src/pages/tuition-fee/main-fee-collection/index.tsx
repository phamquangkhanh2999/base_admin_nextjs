import Head from 'next/head';
import React from 'react';
import BreadcrumbComponent from '@/components/breadcrumb-component/breadcrumb-component';
import { MainLayoutHome } from '@/components/layouts/main-layout/main-layout';
import { IBreadcrumb } from '@/models/common';

const dataHeadTile = { title: 'Thu phí chính khóa' };
const dataBreadcrumb: IBreadcrumb[] = [];
const MainFeeCollectionPage = () => {
  return (
    <React.Fragment>
      <Head>
        <title>{dataHeadTile.title}</title>
        <meta name="description" content={dataHeadTile.title} />
      </Head>
      <BreadcrumbComponent data={dataBreadcrumb} />
      <h1>Thu phí chính khóa</h1>
    </React.Fragment>
  );
};
MainFeeCollectionPage.Layout = MainLayoutHome;
export default MainFeeCollectionPage;
