import React, { useEffect } from 'react';
import Router from 'next/router';
import Head from 'next/head';
import { Button } from 'antd';
import { NextPageWithLayout } from '@/models/common';
import { MainLayoutHome } from '@/components/layouts/main-layout/main-layout';
import HomePage from './home-page';

const Home: NextPageWithLayout = (props) => {
  useEffect(() => {
    const { pathname } = Router;
    if (pathname == '/') {
      Router.push('/home-page');
    }
  }, [props]);
  return (
    <>
      <Head>
        <title>Web admin</title>
        <meta name="description" content="Kidden Garden" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      {/* <HomePage/> */}
    </>
  );
};

Home.Layout = MainLayoutHome;
export default Home;
