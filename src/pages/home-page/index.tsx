import Head from 'next/head';
import React from 'react';
import BreadcrumbComponent from '@/components/breadcrumb-component/breadcrumb-component';
import { MainLayoutHome } from '@/components/layouts/main-layout/main-layout';
import { IBreadcrumb } from '@/models/common';

const dataBreadcrumb: IBreadcrumb[] = [];
const dataHeadTile = { title: 'Trang chủ' };

const HomePage = () => {
  return (
    <React.Fragment>
      <Head>
        <title>{dataHeadTile.title}</title>
        <meta name="description" content={dataHeadTile.title} />
      </Head>
      <BreadcrumbComponent data={dataBreadcrumb} />
    </React.Fragment>
  );
};
HomePage.Layout = MainLayoutHome;
export default HomePage;
