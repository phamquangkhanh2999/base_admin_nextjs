import Head from 'next/head';
import React, { useMemo, useState } from 'react';
import BreadcrumbComponent from '@/components/breadcrumb-component/breadcrumb-component';
import { MainLayoutHome } from '@/components/layouts/main-layout/main-layout';
import { IBreadcrumb, IPagination } from '@/models/common';
import CardComponent from '@/components/card-component/card-component';
import { Col, Row, Space, Typography, Modal, Button } from 'antd';
import {
  ButtonPrimary,
  FontBold,
  PaginationWrapper,
  TableCustoms,
  WrapperTable,
} from '@/components/styled-components/common-styled';
import {
  StyleTitleWrapper,
  TitleStyle,
} from '@/components/breadcrumb-component/breadcrumb-component.styled';
import { DeleteOutlined, EditOutlined, PlusOutlined } from '@ant-design/icons';
import { ColumnsType } from 'antd/es/table';
import { useMediaQuery } from 'react-responsive';
import PaginationComponent from '@/components/pagination';
import { useRouter } from 'next/router';
import ModalComponent from '@/components/ModalComponent';
const { Text } = Typography;
const dataBreadcrumb: IBreadcrumb[] = [{ name: 'Thông tin trường học' }];
const dataHeadTile = { title: 'Thông tin trường học' };

const dataTable: any = [
  {
    id: 1,
    code: 'CN01',
    branch_name: 'Trường mầm non Kidden Garden chi nhánh 1',
    phone: '0989337480',
    email: 'Kiddengarden@edu.com.vn',
    address: '20 Tràng Thi, Hoàn Kiếm, Hà Nội',
    status: 1,
  },
  {
    id: 2,
    code: 'CN02',
    branch_name: 'Trường mầm non Kidden Garden chi nhánh 3',
    phone: '0989337480',
    email: 'Kiddengarden@edu.com.vn',
    address: '20 Tràng Thi, Hoàn Kiếm, Hà Nội',
    status: 1,
  },
  {
    id: 3,
    code: 'CN03',
    branch_name: 'Trường mầm non Kidden Garden chi nhánh 3',
    phone: '0989337480',
    email: 'Kiddengarden@edu.com.vn',
    address: '20 Tràng Thi, Hoàn Kiếm, Hà Nội',
    status: 0,
  },
];
const checkStatus = (item: number) => {
  return item === 1 ? (
    <Text type="success">Đang sử dụng</Text>
  ) : (
    <Text type="danger">Chưa sử dụng</Text>
  );
};
const SchoolInformationPage = () => {
  const router = useRouter();
  const isMobile = useMediaQuery({ maxWidth: 768 });
  const [pagination, setPagination] = useState<IPagination>({ page: 1, perPage: 10 });
  const [modalOpen, setModalOpen] = useState<boolean>(false);
  const columns: ColumnsType<any> = useMemo(() => {
    return [
      {
        dataIndex: 'stt',
        title: 'STT',
        align: 'center',
        width: 60,
        render: (text, record, index) => index + 1,
      },
      {
        dataIndex: 'code',
        key: 'code',
        title: 'Mã',
      },
      {
        dataIndex: 'branch_name',
        key: 'branch_name',
        title: 'Tên chi nhánh',
      },
      {
        dataIndex: 'phone',
        key: 'phone',
        title: 'Số điện thoại',
      },
      {
        dataIndex: 'email',
        key: 'email',
        title: 'Email',
      },
      {
        dataIndex: 'address',
        key: 'Địa chỉ',
        title: 'STT',
      },
      {
        dataIndex: 'status',
        key: 'status',
        title: 'Trạng thái',
        render: (text) => checkStatus(text),
      },
      {
        dataIndex: 'action',
        key: 'action',
        title: 'Hành động',
        align: 'center',
        render: (_, record) => (
          <Space>
            <EditOutlined className="icon-action" onClick={() => handleRecordEdit(record)} />
            <DeleteOutlined className="icon-delete" onClick={() => showModalDelete(record)} />
          </Space>
        ),
      },
    ];
  }, []);
  const handleRecordEdit = (record: any) => {
    router.push({
      pathname: '/school-information/[id]',
      query: {
        id: record.branch_name,
      },
    });
  };
  const handleCreateNew = () => {
    router.push({
      pathname: '/school-information/[id]',
      query: {
        id: 'new',
      },
    });
  };
  const handleConfirm = () => {
    console.log('ahihi');
  };
  const showModalDelete = (record: any) => {
    setModalOpen(true);
  };
  // Pagination
  const onChangePagination = (pagination: IPagination) => {
    setPagination(pagination);
    // let payload = commons.generateParams(
    //   pagination,
    //   {},
    //   String(router.query?.id)
    // )
    // dispatch(getListUserDepartment(payload))
  };
  return (
    <React.Fragment>
      {/* Title Page */}
      <Head>
        <title>{dataHeadTile.title}</title>
        <meta name="description" content={dataHeadTile.title} />
      </Head>
      {/* Breadcrumb */}
      <BreadcrumbComponent
        data={dataBreadcrumb}
        titlePage="Thông tin trường học"
        visibleButtonNew={false}
      />
      {/* cart info */}
      <CardComponent cartTitle="Thông tin trường học">
        <Row gutter={[16, 25]}>
          <Col xxl={12} xl={12} sm={12} xs={24}>
            <Row gutter={[16, 16]}>
              <Col xxl={6} xl={6} sm={6} xs={24}>
                <FontBold>Tên chi nhánh:</FontBold>
              </Col>
              <Col xxl={12} xl={12} sm={12} xs={24}>
                Trường mầm non Kidden Garden chi nhánh 3
              </Col>
            </Row>
          </Col>
          <Col xxl={12} xl={12} sm={12} xs={24}>
            <Row gutter={[16, 16]}>
              <Col xxl={6} xl={6} sm={6} xs={24}>
                <FontBold>Số điện thoại:</FontBold>
              </Col>
              <Col xxl={12} xl={12} sm={12} xs={24}>
                0989337480
              </Col>
            </Row>
          </Col>
          <Col xxl={12} xl={12} sm={12} xs={24}>
            <Row gutter={[16, 16]}>
              <Col xxl={6} xl={6} sm={6} xs={24}>
                <FontBold>Địa chỉ:</FontBold>
              </Col>
              <Col xxl={12} xl={12} sm={12} xs={24}>
                20 Tràng Thi, Hoàn Kiếm, Hà Nội
              </Col>
            </Row>
          </Col>
          <Col xxl={12} xl={12} sm={12} xs={24}>
            <Row gutter={[16, 16]}>
              <Col xxl={6} xl={6} sm={6} xs={24}>
                <FontBold>Email:</FontBold>
              </Col>
              <Col xxl={12} xl={12} sm={12} xs={24}>
                Kiddengarden@edu.com.vn
              </Col>
            </Row>
          </Col>
        </Row>
      </CardComponent>
      {/* table school */}

      <StyleTitleWrapper>
        <TitleStyle level={1}>Chi nhánh trường học</TitleStyle>
        <Space>
          <ButtonPrimary
            size="large"
            type="primary"
            icon={<PlusOutlined />}
            onClick={handleCreateNew}
          >
            Tạo mới
          </ButtonPrimary>
        </Space>
      </StyleTitleWrapper>
      <WrapperTable>
        <TableCustoms
          rowKey={(record: any) => record.id}
          columns={columns}
          // loading={loading}
          dataSource={dataTable}
          pagination={false}
          scroll={isMobile ? { x: 1260 } : { x: undefined }}
        />
        <PaginationWrapper>
          <PaginationComponent
            totalCount={50}
            onChangePagination={onChangePagination}
            pageSize={pagination.perPage}
            current={pagination.page}
          />
        </PaginationWrapper>
      </WrapperTable>
      <ModalComponent
        isModalOpen={modalOpen}
        handleConfirm={handleConfirm}
        loadingBtn={false}
        titleHeader={'Xác nhận'}
        closeModalOpen={() => setModalOpen(false)}
        text={'Bạn chắc chắn muốn xóa chi nhánh này?'}
        textBtn={'Xác nhận'}
        isCheckDelete={true}
      />
    </React.Fragment>
  );
};
SchoolInformationPage.Layout = MainLayoutHome;
export default SchoolInformationPage;
