import React, { useMemo, useState } from 'react';
import BreadcrumbComponent from '@/components/breadcrumb-component';
import CardComponent from '@/components/card-component';
import { MainLayoutHome } from '@/components/layouts/main-layout/main-layout';
import { MessageNotification, RouterId } from '@/config/constants';
import { IBreadcrumb } from '@/models/common';
import { Col, Form, Input, Row, Select } from 'antd';
import _ from 'lodash';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { DownOutlined } from '@ant-design/icons';
import CommonButtons from '@/components/commom-buttons';

const { Option } = Select;
const dataHeadTile = { title: 'Tạo mới chi nhánh' };

const SchoolInformationNewPage = () => {
  const router = useRouter();
  const { id } = router.query;
  const [form] = Form.useForm();
  const [btnDisabled, setBtnDisabled] = useState<boolean>(true);

  const dataBreadcrumb: IBreadcrumb[] = useMemo(() => {
    if (id === RouterId.New) {
      let dataBreadcrumbNew: IBreadcrumb[] = [
        { name: 'Thông tin trường học', routeName: '/school-information' },
        { name: 'Tạo chi nhánh' },
      ];
      return dataBreadcrumbNew;
    } else {
      let dataBreadcrumbEdit: IBreadcrumb[] = [
        { name: 'Thông tin trường học', routeName: '/school-information' },
        { name: String(id) },
      ];
      return dataBreadcrumbEdit;
    }
  }, [id]);
  const onFinish = (values: any) => {
    console.log(values);
  };
  const onValuesChange = (changedValues: any, allValues: any) => {
    if (!_.isEmpty(allValues.title) || !_.isEmpty(allValues.description)) {
      setBtnDisabled(false);
    } else {
      setBtnDisabled(true);
    }
  };
  return (
    <>
      <Head>
        <title>{id === RouterId.New ? dataHeadTile.title : id}</title>
        <meta name="description" content={id === RouterId.New ? dataHeadTile.title : String(id)} />
      </Head>
      <BreadcrumbComponent
        data={dataBreadcrumb}
        titlePage={id === RouterId.New ? 'Tạo mới chi nhánh' : 'Chỉnh sửa chi nhánh'}
        visibleButtonNew={false}
      />
      <Form
        name="form-new-school"
        form={form}
        onFinish={onFinish}
        layout="vertical"
        onValuesChange={onValuesChange}
      >
        <CardComponent cartTitle="Thông tin chi nhánh">
          <Row gutter={[32, 16]}>
            <Col xxl={24} xl={24} sm={24} xs={24}>
              <Form.Item
                label={'Tên chi nhánh'}
                name={'title'}
                rules={[
                  {
                    required: true,
                    message: `Tên chi nhánh ${MessageNotification.messageRequire}`,
                  },
                ]}
              >
                <Input placeholder="Tên chi nhánh" />
              </Form.Item>
            </Col>
            <Col xxl={12} xl={12} sm={12} xs={24}>
              <Form.Item
                label={'Số điện thoại'}
                name={'phone'}
                rules={[
                  {
                    required: true,
                    message: `Số điện thoại ${MessageNotification.messageRequire}`,
                  },
                ]}
              >
                <Input type="number" placeholder="Số điện thoại" />
              </Form.Item>
            </Col>
            <Col xxl={12} xl={12} sm={12} xs={24}>
              <Form.Item
                label={'Email'}
                name={'title'}
                rules={[{ required: true, message: `Email ${MessageNotification.messageRequire}` }]}
              >
                <Input placeholder="Email" />
              </Form.Item>
            </Col>
            <Col xxl={12} xl={12} sm={12} xs={24}>
              <Form.Item
                label={'Địa chỉ'}
                name={'address'}
                rules={[
                  { required: true, message: `Địa chỉ  ${MessageNotification.messageRequire}` },
                ]}
              >
                <Input placeholder="Địa chỉ " />
              </Form.Item>
            </Col>
            <Col xxl={6} xl={6} sm={6} xs={24}>
              <Form.Item label={' '} name={'address'}>
                <Select
                  suffixIcon={<DownOutlined className="color-icon" />}
                  placeholder="Địa chỉ"
                  // onChange={onGenderChange}
                  allowClear
                >
                  <Option value="male">Hà nội</Option>
                  <Option value="female">Thái bình</Option>
                  <Option value="other">Hưng yên</Option>
                </Select>
              </Form.Item>
            </Col>
            <Col xxl={6} xl={6} sm={6} xs={24}>
              <Form.Item label={' '} name={'address'}>
                <Select
                  suffixIcon={<DownOutlined className="color-icon" />}
                  placeholder="Địa chỉ"
                  // onChange={onGenderChange}
                  allowClear
                >
                  <Option value="male">Hà nội</Option>
                  <Option value="female">Thái bình</Option>
                  <Option value="other">Hưng yên</Option>
                </Select>
              </Form.Item>
            </Col>
            <Col xxl={12} xl={12} sm={12} xs={24}>
              <Form.Item
                label={'Trạng thái '}
                name={'status'}
                rules={[
                  { required: true, message: `Trạng thái ${MessageNotification.messageRequire}` },
                ]}
              >
                <Select
                  suffixIcon={<DownOutlined className="color-icon" />}
                  placeholder="Trạng thái "
                  // onChange={onGenderChange}
                  allowClear
                >
                  <Option value="male">Hà nội</Option>
                  <Option value="female">Thái bình</Option>
                  <Option value="other">Hưng yên</Option>
                </Select>
              </Form.Item>
            </Col>
          </Row>
        </CardComponent>

        <CommonButtons
          titleBtnNew={id === RouterId.New ? 'Tạo mới' : 'Cập nhật'}
          loading={false}
          disabled={false}
        />
      </Form>
    </>
  );
};
SchoolInformationNewPage.Layout = MainLayoutHome;
export default SchoolInformationNewPage;
