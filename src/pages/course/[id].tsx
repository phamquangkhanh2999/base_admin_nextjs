import React, { useMemo, useState } from 'react';
import BreadcrumbComponent from '@/components/breadcrumb-component';
import CardComponent from '@/components/card-component';
import { MainLayoutHome } from '@/components/layouts/main-layout/main-layout';
import { MessageNotification, RouterId } from '@/config/constants';
import { IBreadcrumb } from '@/models/common';
import { Col, ConfigProvider, DatePicker, Form, Input, Row, Select } from 'antd';
import _ from 'lodash';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { DownOutlined } from '@ant-design/icons';
import CommonButtons from '@/components/commom-buttons';
import viVN from 'antd/lib/locale/vi_VN';
import ModalComponent from '@/components/ModalComponent';

const { Option } = Select;
const addressUrl: string = '/course';
const dataHeadTile = { title: 'Tạo mới khóa học' };

const CourseNewPage = () => {
  const router = useRouter();
  const { id } = router.query;
  const [form] = Form.useForm();
  const [btnDisabled, setBtnDisabled] = useState<boolean>(false);
  const [modalOpen, setModalOpen] = useState<boolean>(false);

  const dataBreadcrumb: IBreadcrumb[] = useMemo(() => {
    if (id === RouterId.New) {
      let dataBreadcrumbNew: IBreadcrumb[] = [
        { name: 'Khóa học', routeName: addressUrl },
        { name: 'Tạo mới khóa học' },
      ];
      return dataBreadcrumbNew;
    } else {
      let dataBreadcrumbEdit: IBreadcrumb[] = [
        { name: 'Khóa học', routeName: addressUrl },
        { name: String(id) },
      ];
      return dataBreadcrumbEdit;
    }
  }, [id]);
  const onFinish = (values: any) => {
    console.log(values);
  };
  const onValuesChange = (changedValues: any, allValues: any) => {
    if (!_.isEmpty(allValues.title) || !_.isEmpty(allValues.description)) {
      setBtnDisabled(false);
    } else {
      setBtnDisabled(true);
    }
  };
  const handleChangeStatus = () => {
    setModalOpen(true);
  };
  const handleConfirm = () => {
    console.log('ahihi');
  };
  return (
    <>
      <Head>
        <title>{id === RouterId.New ? dataHeadTile.title : id}</title>
        <meta name="description" content={id === RouterId.New ? dataHeadTile.title : String(id)} />
      </Head>
      <BreadcrumbComponent
        data={dataBreadcrumb}
        titlePage={id === RouterId.New ? 'Tạo mới khóa học' : 'Chỉnh sửa khóa học'}
        visibleButtonNew={false}
      />
      <Form
        name="form-new-school"
        form={form}
        onFinish={onFinish}
        layout="vertical"
        onValuesChange={onValuesChange}
      >
        <CardComponent cartTitle="Thông tin khóa học">
          <Row gutter={[12, 16]}>
            <Col xxl={12} xl={12} sm={12} xs={24}>
              <Row>
                <Col xxl={24} xl={24} sm={24} xs={24}>
                  <Form.Item label={'Loại khóa '} name={'title'}>
                    <Input placeholder="Loại khóa" />
                  </Form.Item>
                </Col>
                <Col xxl={24} xl={24} sm={24} xs={24}>
                  <ConfigProvider locale={viVN}>
                    <Form.Item
                      label={'Ngày bắt đầu'}
                      name={'dateStart'}
                      rules={[
                        {
                          required: true,
                          message: `Ngày bắt đầu ${MessageNotification.messageRequire}`,
                        },
                      ]}
                    >
                      <DatePicker
                        allowClear={false}
                        placeholder="Ngày bắt đầu"
                        className="datePicker"
                      />
                    </Form.Item>
                  </ConfigProvider>
                </Col>
                <Col xxl={24} xl={24} sm={24} xs={24}>
                  <ConfigProvider locale={viVN}>
                    <Form.Item
                      label={'Ngày kết thúc'}
                      name={'dateEnd'}
                      rules={[
                        {
                          required: true,
                          message: `Ngày kết thúc ${MessageNotification.messageRequire}`,
                        },
                      ]}
                    >
                      <DatePicker
                        allowClear={false}
                        placeholder="Ngày kết thúc"
                        className="datePicker"
                      />
                    </Form.Item>
                  </ConfigProvider>
                </Col>
                <Col xxl={24} xl={24} sm={24} xs={24}>
                  <Form.Item label={'Trạng thái '} name={'status'}>
                    <Select
                      suffixIcon={<DownOutlined className="color-icon" />}
                      placeholder="Trạng thái "
                      // onChange={onGenderChange}
                      allowClear
                    >
                      <Option value="male">Hà nội</Option>
                      <Option value="female">Thái bình</Option>
                      <Option value="other">Hưng yên</Option>
                    </Select>
                  </Form.Item>
                </Col>
              </Row>
            </Col>
            <Col xxl={12} xl={12} sm={12} xs={24}></Col>
          </Row>
        </CardComponent>

        <CommonButtons
          titleBtnNew={id === RouterId.New ? 'Tạo mới' : 'Cập nhật'}
          loading={false}
          disabled={btnDisabled}
          bgColor="success"
          textBtnStatus={id === RouterId.New ? null : 'Open'}
          handleChangeStatus={handleChangeStatus}
        />
      </Form>
      <ModalComponent
        isModalOpen={modalOpen}
        handleConfirm={handleConfirm}
        loadingBtn={false}
        titleHeader={'Xác nhận'}
        closeModalOpen={() => setModalOpen(false)}
        text={'Bạn chắc chắn muốn mở khóa học này?'}
        textBtn={'Xác nhận'}
        isCheckDelete={true}
      />
    </>
  );
};
CourseNewPage.Layout = MainLayoutHome;
export default CourseNewPage;
