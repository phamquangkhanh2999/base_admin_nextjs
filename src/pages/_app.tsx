import '../styles/globals.css';
import Head from 'next/head';
import { Provider } from 'react-redux';
import { CacheProvider } from '@emotion/react';
import { ThemeProvider } from 'styled-components';
// import { createEmotionCache } from '@/utils';
import { AppPropsWithLayout } from '@/models/common';
import { Fragment } from 'react';
import { EmptyLayout } from '@/components/layouts';
import { themeDefault } from '@/components/theme';
import { store } from '@/app/store';
import { createEmotionCache } from '../utils';
import { ConfigProvider } from 'antd';
import viVN from 'antd/lib/locale/vi_VN';
const clientSideEmotionCache = createEmotionCache();

export default function App({ Component, pageProps }: AppPropsWithLayout) {
  const Layout = Component.Layout ?? EmptyLayout;

  return (
    <Fragment>
      <Head>
        <title>Kidden Garden</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Provider store={store}>
        <ThemeProvider theme={themeDefault}>
          <CacheProvider value={clientSideEmotionCache}>
            <ConfigProvider locale={viVN}>
              <Layout>
                <Component {...pageProps} />
              </Layout>
            </ConfigProvider>
          </CacheProvider>
        </ThemeProvider>
      </Provider>
    </Fragment>
  );
}
