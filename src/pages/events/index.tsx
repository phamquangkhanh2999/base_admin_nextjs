import Head from 'next/head';
import React, { useMemo, useState } from 'react';
import BreadcrumbComponent from '@/components/breadcrumb-component/breadcrumb-component';
import { MainLayoutHome } from '@/components/layouts/main-layout/main-layout';
import { IBreadcrumb, IPagination } from '@/models/common';
import { Space, Typography, Col, Input, Select } from 'antd';
import {
  PaginationWrapper,
  TableCustoms,
  WrapperTable,
} from '@/components/styled-components/common-styled';
import { DeleteOutlined, EditOutlined, SearchOutlined } from '@ant-design/icons';
import { ColumnsType } from 'antd/es/table';
import { useMediaQuery } from 'react-responsive';
import PaginationComponent from '@/components/pagination';
import { useRouter } from 'next/router';
import ModalComponent from '@/components/ModalComponent';
import moment from 'moment';
import { FormatDate, FormatTime } from '@/config/constants';
import SearchComponent from '@/components/search-component';
import { cloneDeep } from 'lodash';

const { Text } = Typography;
const addressUrl: string = '/events/[id]';
const initFilter = {};
const initPagination: IPagination = {
  page: 1,
  perPage: 10,
};
const dataBreadcrumb: IBreadcrumb[] = [{ name: 'Sự kiện' }];
const dataHeadTile = { title: 'Danh sách sự kiện' };
const checkStatus: any = {
  0: <Text type="danger">Closed</Text>,
  1: <Text type="warning">Draft</Text>,
  2: <Text type="success">Opening</Text>,
};
const dataTable: any = [
  {
    id: 1,
    code: 'CN01',
    branch_name: 'Chính khóa',
    status: 0,
    dateStart: '2023-09-28T09:00:42Z',
    timeEnd: '2023-11-28T09:00:42Z',
    expense: 100000,
    objectJoin: 'Khối lớp Mầm',
  },
  {
    id: 2,
    code: 'CN02',
    status: 1,
    branch_name: 'Chính khóa',
    dateStart: '2023-09-28T09:00:42Z',
    timeEnd: '2023-11-28T09:00:42Z',
    expense: 200000,
    objectJoin: 'Tất cả các bé',
  },
  {
    id: 3,
    code: 'CN03',
    status: 2,
    branch_name: 'Chính khóa',
    dateStart: '2023-09-28T09:00:42Z',
    timeEnd: '2023-11-28T09:00:42Z',
    expense: 100000,
    objectJoin: 'Khối lớp Mầm',
  },
];
const dataSelect = [
  {
    name: 'Closed',
    status: 0,
  },
  {
    name: 'Draft',
    status: 1,
  },
  {
    name: 'Opening',
    status: 2,
  },
];

const EventPage = () => {
  const router = useRouter();
  const isMobile = useMediaQuery({ maxWidth: 768 });
  const [filterData, setFilterData] = useState<any>(cloneDeep(initFilter));
  const [pagination, setPagination] = useState<IPagination>(cloneDeep(initPagination));
  const [modalOpen, setModalOpen] = useState<boolean>(false);
  const columns: ColumnsType<any> = useMemo(() => {
    return [
      {
        dataIndex: 'stt',
        title: 'STT',
        align: 'center',
        width: 60,
        render: (text, record, index) => index + 1,
      },
      {
        dataIndex: 'code',
        key: 'code',
        title: 'Mã',
        render: (text: string, record) => (
          <Text onClick={() => handleRecordEdit(record)} className="router_active">
            {text}
          </Text>
        ),
      },
      {
        dataIndex: 'branch_name',
        key: 'branch_name',
        title: 'Tên sự kiện',
      },
      {
        dataIndex: 'expense',
        key: 'expense',
        title: 'Chi phí',
        render:(text) =>text ? text.toLocaleString('de-DE') : 0
      },
      {
        dataIndex: 'status',
        key: 'status',
        title: 'Trạng thái',
        render: (text) => checkStatus[text],
      },
      {
        dataIndex: 'dateStart',
        key: 'dateStart',
        title: 'Ngày bắt đầu',
        render: (text) => (text ? moment(text).format(FormatDate) : ''),
      },
      {
        dataIndex: 'dateStart',
        key: 'dateStart',
        title: 'Giờ bắt đầu',
        render: (text) => (text ? moment(text).format(FormatTime) : ''),
      },
      {
        dataIndex: 'timeEnd',
        key: 'timeEnd',
        title: 'Giờ kết thúc',
        render: (text) => (text ? moment(text).format(FormatTime) : ''),
      },
      {
        dataIndex: 'objectJoin',
        key: 'objectJoin',
        title: 'Đối tượng tham gia',
      },
      {
        dataIndex: 'action',
        key: 'action',
        title: 'Hành động',
        align: 'center',
        render: (_, record) => (
          <Space>
            {record.status !== 0 && (
              <EditOutlined className="icon-action" onClick={() => handleRecordEdit(record)} />
            )}
            {record.status === 0 && (
              <DeleteOutlined className="icon-delete" onClick={() => showModalDelete(record)} />
            )}
          </Space>
        ),
      },
    ];
  }, []);
  const handleRecordEdit = (record: any) => {
    router.push({
      pathname: addressUrl,
      query: {
        id: record.branch_name,
      },
    });
  };
  const handleCreateNew = () => {
    router.push({
      pathname: addressUrl,
      query: {
        id: 'new',
      },
    });
  };
  const handleConfirm = () => {
    console.log('ahihi');
  };
  const showModalDelete = (record: any) => {
    setModalOpen(true);
  };
  // Pagination
  const onChangePagination = (pagination: IPagination) => {
    setPagination(pagination);
    // let payload = commons.generateParams(
    //   pagination,
    //   {},
    //   String(router.query?.id)
    // )
    // dispatch(getListUserDepartment(payload))
  };
  //onChange input
  const onChangeInput = (value: any, name: string) => {
    setFilterData({
      ...filterData,
      [name]: value,
    });
  };
  // Tìm kiếm
  const onHandleSearch = () => {
    setPagination(cloneDeep(initPagination));
    console.log('Tìm kiếm');
  };
  // Reload
  const onHandleReload = () => {
    setPagination(cloneDeep(initPagination));
    setFilterData(cloneDeep(initFilter));
    console.log('Reload');

    // fetchApiGetCatalog(pagination, newFilter);
  };
  return (
    <React.Fragment>
      {/* Title Page */}
      <Head>
        <title>{dataHeadTile.title}</title>
        <meta name="description" content={dataHeadTile.title} />
      </Head>
      {/* Breadcrumb */}
      <BreadcrumbComponent
        data={dataBreadcrumb}
        titlePage={dataHeadTile.title}
        visibleButtonNew={true}
        handleCreateNew={handleCreateNew}
      />
      {/* search  */}
      <SearchComponent colProps={4} onHandleSearch={onHandleSearch} onHandleReload={onHandleReload}>
        <Col xs={4} xxl={4} xl={4} sm={24}>
          <Input
            onChange={(e) => onChangeInput(e.target.value, 'Name')}
            size="large"
            placeholder="Tên loại khóa"
            value={filterData['Name']}
            prefix={<SearchOutlined />}
          />
        </Col>
        <Col xs={4} xxl={4} xl={4} sm={24}>
          <Select
            showSearch
            allowClear
            size="large"
            style={{ width: '100%' }}
            placeholder={'Trạng thái'}
            // filterOption={(input: string, option: any) =>
            //   option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0 ||
            //   option.props.value.toLowerCase().indexOf(input.toLowerCase()) >= 0
            // }
            value={filterData['Status']}
            onChange={(e) => onChangeInput(e, 'Status')}
          >
            {dataSelect &&
              dataSelect?.map((item: any) => (
                <Select.Option key={item['status']} value={item['status']}>
                  {item['Name']}
                </Select.Option>
              ))}
          </Select>
        </Col>
        <Col xs={4} xxl={4} xl={4} sm={24}>
          <Select
            showSearch
            allowClear
            size="large"
            style={{ width: '100%' }}
            placeholder={'Đối tượng tham gia'}
            // filterOption={(input: string, option: any) =>
            //   option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0 ||
            //   option.props.value.toLowerCase().indexOf(input.toLowerCase()) >= 0
            // }
            value={filterData['number']}
            onChange={(e) => onChangeInput(e, 'number')}
          >
            {dataSelect &&
              dataSelect?.map((item: any) => (
                <Select.Option key={item['status']} value={item['status']}>
                  {item['Name']}
                </Select.Option>
              ))}
          </Select>
        </Col>
      </SearchComponent>
      {/* table school */}
      <WrapperTable>
        <TableCustoms
          rowKey={(record: any) => record.id}
          columns={columns}
          // loading={loading}
          dataSource={dataTable}
          pagination={false}
          scroll={isMobile ? { x: 1260 } : { x: undefined }}
        />
        <PaginationWrapper>
          <PaginationComponent
            totalCount={50}
            onChangePagination={onChangePagination}
            pageSize={pagination.perPage}
            current={pagination.page}
          />
        </PaginationWrapper>
      </WrapperTable>
      <ModalComponent
        isModalOpen={modalOpen}
        handleConfirm={handleConfirm}
        loadingBtn={false}
        titleHeader={'Xác nhận'}
        closeModalOpen={() => setModalOpen(false)}
        text={'Bạn chắc chắn muốn xóa Khóa học này?'}
        textBtn={'Xóa'}
        isCheckDelete={true}
      />
    </React.Fragment>
  );
};
EventPage.Layout = MainLayoutHome;
export default EventPage;
