import React, { useMemo, useState } from 'react';
import BreadcrumbComponent from '@/components/breadcrumb-component';
import CardComponent from '@/components/card-component';
import { MainLayoutHome } from '@/components/layouts/main-layout/main-layout';
import { MessageNotification, RouterId } from '@/config/constants';
import { IBreadcrumb } from '@/models/common';
import { Checkbox, Col, DatePicker, Form, Input, Row, Select } from 'antd';
import _ from 'lodash';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { DownOutlined } from '@ant-design/icons';
import CommonButtons from '@/components/commom-buttons';
import viVN from 'antd/lib/locale/vi_VN';
import ModalComponent from '@/components/ModalComponent';
const { Option } = Select;
const addressUrl: string = '/events';
const dataHeadTile = { title: 'Tạo mới sự kiện' };

const EventNewPage = () => {
  const router = useRouter();
  const { id } = router.query;
  const [form] = Form.useForm();
  const [btnDisabled, setBtnDisabled] = useState<boolean>(false);
  const [modalOpen, setModalOpen] = useState<boolean>(false);
  const [dirty, setDirty] = useState(false);
  const [selectedOptions, setSelectedOptions] = useState(['john']);

  const dataBreadcrumb: IBreadcrumb[] = useMemo(() => {
    if (id === RouterId.New) {
      let dataBreadcrumbNew: IBreadcrumb[] = [
        { name: 'Sự kiện', routeName: addressUrl },
        { name: 'Tạo mới sự kiện' },
      ];
      return dataBreadcrumbNew;
    } else {
      let dataBreadcrumbEdit: IBreadcrumb[] = [
        { name: 'Sự kiện', routeName: addressUrl },
        { name: String(id) },
      ];
      return dataBreadcrumbEdit;
    }
  }, [id]);
  const onFinish = (values: any) => {
    console.log(values);
  };
  const onValuesChange = (changedValues: any, allValues: any) => {
    if (!_.isEmpty(allValues.title) || !_.isEmpty(allValues.description)) {
      setBtnDisabled(false);
    } else {
      setBtnDisabled(true);
    }
  };
  const handleChangeStatus = () => {
    setModalOpen(true);
  };
  const handleConfirm = () => {
    console.log('ahihi');
  };
  const handleChangeSelect = (value: string[]) => {
    setSelectedOptions(value);
    setDirty(true);
    console.log('Select Changed');
  };
  return (
    <>
      <Head>
        <title>{id === RouterId.New ? dataHeadTile.title : id}</title>
        <meta name="description" content={id === RouterId.New ? dataHeadTile.title : String(id)} />
      </Head>
      <BreadcrumbComponent
        data={dataBreadcrumb}
        titlePage={id === RouterId.New ? 'Tạo mới sự kiện' : 'Chỉnh sửa Sự kiện'}
        visibleButtonNew={false}
      />
      <Form
        name="form-new-school"
        form={form}
        onFinish={onFinish}
        layout="vertical"
        onValuesChange={onValuesChange}
      >
        <CardComponent cartTitle="Thông tin Sự kiện">
          <Row gutter={[12, 16]}>
            <Col xxl={12} xl={12} sm={12} xs={24}>
              <Row gutter={[16, 16]}>
                <Col xxl={24} xl={24} sm={24} xs={24}>
                  <Form.Item
                    label={'Tên sự kiện'}
                    name={'title'}
                    rules={[
                      {
                        required: true,
                        message: `Tên sự kiện ${MessageNotification.messageRequire}`,
                      },
                    ]}
                  >
                    <Input placeholder="Tên sự kiện" />
                  </Form.Item>
                </Col>
                <Col xxl={12} xl={12} sm={12} xs={24}>
                  <Form.Item
                    label={'Ngày bắt đầu'}
                    name={'dateStart'}
                    rules={[
                      {
                        required: true,
                        message: `Ngày bắt đầu ${MessageNotification.messageRequire}`,
                      },
                    ]}
                  >
                    <DatePicker
                      allowClear={false}
                      placeholder="Ngày bắt đầu"
                      className="datePicker"
                    />
                  </Form.Item>
                </Col>
                <Col xxl={12} xl={12} sm={12} xs={24}>
                  <Form.Item
                    label={'Giờ bắt đầu'}
                    name={'dateEnd'}
                    rules={[
                      {
                        required: true,
                        message: `Giờ bắt đầu ${MessageNotification.messageRequire}`,
                      },
                    ]}
                  >
                    <Input type="time" placeholder="Giờ bắt đầu " />
                  </Form.Item>
                </Col>
                <Col xxl={12} xl={12} sm={12} xs={24}></Col>
                <Col xxl={12} xl={12} sm={12} xs={24}>
                  <Form.Item
                    label={'Giờ kết thúc'}
                    name={'dateEnd'}
                    rules={[
                      {
                        required: true,
                        message: `Giờ kết thúc ${MessageNotification.messageRequire}`,
                      },
                    ]}
                  >
                    <Input
                      type="time"
                      placeholder="Giờ kết thúc"
                    />
                  </Form.Item>
                </Col>
                <Col xxl={24} xl={24} sm={24} xs={24}>
                  <Form.Item
                    label={'Yêu cầu đăng ký'}
                    name={'status'}
                    rules={[
                      {
                        required: true,
                        message: `Yêu cầu đăng ký ${MessageNotification.messageRequire}`,
                      },
                    ]}
                  >
                    <Select
                      suffixIcon={<DownOutlined className="color-icon" />}
                      placeholder="Yêu cầu đăng ký"
                      // onChange={onGenderChange}
                      allowClear
                    >
                      <Option value="male">Hà nội</Option>
                      <Option value="female">Thái bình</Option>
                      <Option value="other">Hưng yên</Option>
                    </Select>
                  </Form.Item>
                </Col>
                <Col xxl={24} xl={24} sm={24} xs={24}>
                  <Form.Item label={'Mô tả sự kiện'} name={'dateEnd'}>
                    <Input.TextArea rows={5} placeholder="Mô tả sự kiện" />
                  </Form.Item>
                </Col>
              </Row>
            </Col>
            <Col xxl={12} xl={12} sm={12} xs={24}>
              <Row gutter={[16, 16]}>
                <Col xxl={24} xl={24} sm={24} xs={24}>
                  <Form.Item
                    label={'Địa điểm'}
                    name={'title'}
                    rules={[
                      {
                        required: true,
                        message: `Địa điểm ${MessageNotification.messageRequire}`,
                      },
                    ]}
                  >
                    <Input placeholder="Địa điểm" />
                  </Form.Item>
                </Col>
                <Col xxl={24} xl={24} sm={24} xs={24}>
                  <Form.Item
                    label={'Chi phí'}
                    name={'title'}
                    rules={[
                      {
                        required: true,
                        message: `Chi phí ${MessageNotification.messageRequire}`,
                      },
                    ]}
                  >
                    <Input type="number" placeholder="Chi phí" />
                  </Form.Item>
                </Col>
                <Col xxl={24} xl={24} sm={24} xs={24}>
                  <Form.Item
                    label={'Hạn đăng ký'}
                    name={'dateStart'}
                    rules={[
                      {
                        required: true,
                        message: `Hạn đăng ký ${MessageNotification.messageRequire}`,
                      },
                    ]}
                  >
                    <DatePicker
                      allowClear={false}
                      placeholder="Hạn đăng ký"
                      className="datePicker"
                    />
                  </Form.Item>
                </Col>
                <Col xxl={24} xl={24} sm={24} xs={24}>
                  <Form.Item
                    label={'Đối tượng tham gia'}
                    name={'status12'}
                    rules={[
                      {
                        required: true,
                        message: `Đối tượng tham gia ${MessageNotification.messageRequire}`,
                      },
                    ]}
                  >
                    <Select
                      onChange={handleChangeSelect}
                      onMouseDown={(e) => {
                        setDirty(false);
                        e.stopPropagation();
                      }}
                      mode="multiple"
                      options={[
                        {
                          value: 'john',
                          label: (
                            <Checkbox
                              onClick={(e) => {
                                if (dirty) {
                                  e.stopPropagation();
                                }
                                setDirty(false);
                                console.log('Check Clicked');
                              }}
                              checked={selectedOptions.includes('john')}
                            >
                              Tất cả các bé
                            </Checkbox>
                          ),
                        },
                        {
                          value: 'jim',
                          label: (
                            <Checkbox
                              onClick={(e) => {
                                if (dirty) {
                                  e.stopPropagation();
                                }
                                setDirty(false);
                              }}
                              checked={selectedOptions.includes('jim')}
                            >
                              Khối lớp Mầm
                            </Checkbox>
                          ),
                        },
                        {
                          value: 'johhny',
                          label: (
                            <Checkbox
                              onClick={(e) => {
                                if (dirty) {
                                  e.stopPropagation();
                                }
                                setDirty(false);
                              }}
                              checked={selectedOptions.includes('johhny')}
                            >
                              Khối lớp Lá
                            </Checkbox>
                          ),
                        },
                      ]}
                    />
                  </Form.Item>
                </Col>
              </Row>
            </Col>
          </Row>
        </CardComponent>

        <CommonButtons
          titleBtnNew={id === RouterId.New ? 'Tạo mới' : 'Cập nhật'}
          loading={false}
          disabled={btnDisabled}
          bgColor="success"
          textBtnStatus={id === RouterId.New ? null : 'Open'}
          handleChangeStatus={handleChangeStatus}
        />
      </Form>

      <ModalComponent
        isModalOpen={modalOpen}
        handleConfirm={handleConfirm}
        loadingBtn={false}
        titleHeader={'Xác nhận'}
        closeModalOpen={() => setModalOpen(false)}
        text={'Bạn chắc chắn muốn mở Sự kiện này?'}
        textBtn={'Xác nhận'}
        isCheckDelete={true}
      />
    </>
  );
};
EventNewPage.Layout = MainLayoutHome;
export default EventNewPage;
