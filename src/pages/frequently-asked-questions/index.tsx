import React, { useState } from 'react';
import dynamic from 'next/dynamic';
import { MainLayoutHome } from '@/components/layouts/main-layout/main-layout';
import BreadcrumbComponent from '@/components/breadcrumb-component/breadcrumb-component';
import { IBreadcrumb } from '@/models/common';
import CardComponent from '@/components/card-component/card-component';

const CkeditorComponet = dynamic(() => import('@/components/ckeditor/ckeditor'), {
  ssr: false,
});

const dataBreadcrumb: IBreadcrumb[] = [
  {
    name: 'FAQs',
  },
];

const FrequentlyAskedQuestions = () => {
  const [editor, setEditor] = useState<string>('');

  return (
    <React.Fragment>
      <BreadcrumbComponent data={dataBreadcrumb} titlePage="FAQs" />
      <CardComponent cartTitle="FAQs">
        <CkeditorComponet data={editor} setEditor={setEditor} />
      </CardComponent>
    </React.Fragment>
  );
};
FrequentlyAskedQuestions.Layout = MainLayoutHome;
export default FrequentlyAskedQuestions;
