import React, { useState, Fragment } from 'react';
import Head from 'next/head';
import settings from '@/data/settings-domain.json';
import AuthLayout from '../../../components/layouts/auth-layout/auth-layout';
import { Input, message, Typography } from 'antd';
import { useRouter } from 'next/router';
import { UserOutlined } from '@ant-design/icons';
import Image from 'next/image';
// import { userForgotPassword, userSendOtp } from '@/features/user-slice';
// import { IForgotPasswordPayload, ILoginResponseNotActive } from '@/models/user';
// import { useAppDispatch } from '@/app/hooks';
import ImageLogoLogin from '@/assets/images/illustrationLogin.png';
import {
  ButtonNext,
  InputEmail,
  LogoLogin,
  WrapperAuth,
  WrapperForgot,
  WrapperLogin,
} from '@/components/styled-components/auth-styled-components/AuthStyled';

type Props = {};
interface IAllValues {
  Email: string | undefined;
}

const { Title } = Typography;
const ForgotPassword = (props: Props) => {
  // const dispatch = useAppDispatch();
  const [userName, setUserName] = useState<string>('');
  const router = useRouter();
  const [disable, setDisable] = useState<boolean>(true);
  const handleOnchange = (value: any) => {
    setUserName(value.target.value);
    {
      value.target.value ? setDisable(false) : setDisable(true);
    }
  };
  // call api forgot password
  const handleForgotPassword = () => {
    router.push('/auth/send-otp');
    // let payload: IForgotPasswordPayload = {
    //   User: userName,
    // };
    // dispatch(userForgotPassword(payload))
    //   .unwrap()
    //   .then()
    //   .then((res: any) => {
    //     let payload: ILoginResponseNotActive = {
    //       Token: res.Payload.Token,
    //       TranslateContext: 'Identity',
    //       TranslateKey: 'NotActiveException',
    //     };
    //     dispatch(userSendOtp(payload))
    //       .unwrap()
    //       .then((res: any) => {
    //         router.push({
    //           query: {
    //             user: userName,
    //           },
    //           pathname: '/auth/send-otp',
    //         });
    //       });
    //   })
    //   .catch((error: any) => {
    //     if (error.Message == 'Entity was not found.') {
    //       message.error({
    //         content: 'Email / Số điện thoại không tồn tại.',
    //         className: 'erroNotFound-class',
    //         style: {
    //           marginTop: '3vh',
    //         },
    //       });
    //     }
    //   });
  };

  return (
    <Fragment>
      <Head>
        <title>{settings?.title_login}</title>
      </Head>
      <WrapperAuth>
        <WrapperLogin>
          <LogoLogin>
            <Image src={ImageLogoLogin} alt="Logo login" />
          </LogoLogin>
          <WrapperForgot>
            <Title level={3}>Lấy lại mật khẩu</Title>
            <InputEmail
              prefix={<UserOutlined className="site-form-item-icon" />}
              placeholder="Email/ Số điện thoại"
              size={'large'}
              onChange={handleOnchange}
            />
            <ButtonNext
              type="primary"
              size={'large'}
              onClick={handleForgotPassword}
              disabled={disable}
            >
              Tiếp theo
            </ButtonNext>
            <p onClick={() => router.back()} style={{ textAlign: 'center', cursor: 'pointer' }}>
              Quay lại
            </p>
          </WrapperForgot>
        </WrapperLogin>
      </WrapperAuth>
    </Fragment>
  );
};
ForgotPassword.Layout = AuthLayout;
export default ForgotPassword;
