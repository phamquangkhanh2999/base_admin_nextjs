import React, { Fragment } from 'react';
import Head from 'next/head';
import settings from '@/data/settings-domain.json';
import AuthLayout from '@/components/layouts/auth-layout/auth-layout';
import { Button, Form, Input, message, Typography } from 'antd';
import { useRouter } from 'next/router';
import { LockOutlined, UserOutlined } from '@ant-design/icons';
import {
  WrapperAuth,
  WrapperLogin,
  LogoLogin,
  NewPasswordWrapper,
} from '@/components/styled-components/auth-styled-components/AuthStyled';
import { StyledFormLogin } from '@/components/auth/sign-in/SignInStyled';
import { RegexPassword } from '@/models/common';
import Image from 'next/image';
import ImageLogoLogin from '@/assets/images/illustrationLogin.png';
// import { RegexPassword, ResponseErrorChangePassword } from '../../../models/common';
// import { IResetPasswordPayload } from '@/models/user';
// import { useAppDispatch, useAppSelector } from '@/app/hooks';
// import { selectUser, userResetPassword } from '@/features/user-slice';

const { Title } = Typography;
type Props = {};
const ForgotPassword = (props: Props) => {
  const router = useRouter();
  // const dispatch = useAppDispatch();
  // const {otp, loading } = useAppSelector(selectUser);

  // change password
  const onFinish = (values: any) => {
    // let payload: IResetPasswordPayload = {
    //   Token: otp.Payload.Token,
    //   NewPassword: values.password,
    // };
    // dispatch(userResetPassword(payload))
    //   .unwrap()
    //   .then()
    //   .then((res: any) => {
    //     message.success({
    //       content: ResponseErrorChangePassword.SUCCESSRESETPASS,
    //       className: 'erroNotFound-class',
    //       style: {
    //         marginTop: '3vh',
    //       },
    //     });
    //     router.push('/auth');
    //   });
  };

  const onFinishFailed = (errorInfo: any) => {};
  return (
    <Fragment>
      <Head>
        <title>{settings?.title_login}</title>
      </Head>
      <WrapperAuth>
        <WrapperLogin>
          <LogoLogin>
            <Image src={ImageLogoLogin} alt="Logo login" />
          </LogoLogin>

          <NewPasswordWrapper>
            <Title level={3}>Đổi mật khẩu</Title>
            <StyledFormLogin
              name="basic"
              initialValues={{ remember: true }}
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
              autoComplete="off"
              style={{ marginTop: '38px' }}
            >
              <Form.Item
                name="password"
                label=""
                rules={[
                  { required: true, message: 'Vui lòng nhập mật khẩu của bạn!' },
                  { min: 8, message: 'Mật khẩu phải có độ dài tối thiểu là 8' },
                  {
                    pattern: new RegExp(RegexPassword),
                    message:
                      'Mật khẩu phải chứa ít nhất một chữ cái thường, chữ hoa, số và ký tự đặc biệt',
                  },
                ]}
                hasFeedback
              >
                <Input.Password
                  size="large"
                  prefix={<LockOutlined className="site-form-item-icon" />}
                  placeholder="Mật khẩu mới"
                />
              </Form.Item>

              <Form.Item
                name="confirm"
                label=""
                dependencies={['password']}
                hasFeedback
                rules={[
                  {
                    required: true,
                    message: 'Vui lòng xác nhận mật khẩu của bạn!',
                  },
                  ({ getFieldValue }) => ({
                    validator(_, value) {
                      if (!value || getFieldValue('password') === value) {
                        return Promise.resolve();
                      }
                      return Promise.reject(new Error('Hai mật khẩu bạn đã nhập không khớp!'));
                    },
                  }),
                ]}
              >
                <Input.Password
                  size="large"
                  prefix={<LockOutlined className="site-form-item-icon" />}
                  placeholder="Nhập lại mật khẩu"
                />
              </Form.Item>
              <Button
                size="large"
                style={{
                  width: '100%',
                  borderRadius: '10px',
                  marginBottom: '12px',
                }}
                // loading={loading}
                type={'primary'}
                htmlType={'submit'}
              >
                Tiếp theo
              </Button>
              <p onClick={() => router.back()} style={{ textAlign: 'center', cursor: 'pointer' }}>
                Quay lại
              </p>
            </StyledFormLogin>
          </NewPasswordWrapper>
        </WrapperLogin>
      </WrapperAuth>
    </Fragment>
  );
};
ForgotPassword.Layout = AuthLayout;
export default ForgotPassword;
