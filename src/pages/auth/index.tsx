import React, { Fragment } from 'react';
import Head from 'next/head';
import settings from '@/data/settings-domain.json';

import {
  AuthContent,
  AuthLeft,
  AuthRight,
  WrapperAuth,
} from '@/components/styled-components/auth-styled-components/AuthStyled';
import Image from 'next/image';
import ImageLogoLogin from '@/assets/images/illustrationLogin.png';
import SignIn from '@/components/auth/sign-in';
import AuthLayout from '@/components/layouts/auth-layout/auth-layout';

type Props = {};
const Auth = (props: Props) => {
  return (
    <Fragment>
      <Head>
        <title>{settings?.title_login}</title>
      </Head>
      <WrapperAuth>
        <AuthLeft></AuthLeft>
        <AuthRight>
          <AuthContent>
            <SignIn />
          </AuthContent>
        </AuthRight>
      </WrapperAuth>
    </Fragment>
  );
};
Auth.Layout = AuthLayout;
export default Auth;
