import AuthLayout from '@/components/layouts/auth-layout/auth-layout';
import React, { Fragment, useEffect, useState } from 'react';
import Head from 'next/head';
import settings from '@/data/settings-domain.json';
import { useRouter } from 'next/router';
import { TabletOutlined } from '@ant-design/icons';
import {
  WrapperAuth,
  WrapperLogin,
  LogoLogin,
  SendOtpWrapper,
  ButtonNext,
  InputOtp,
  InputOtpBtn,
  InputOtpWrapper,
  SendOtpSpan,
  SendOtpTop,
} from '@/components/styled-components/auth-styled-components/AuthStyled';
import { Alert, message, Typography } from 'antd';
import { Form } from 'antd';
import Image from 'next/image';
import ImageLogoLogin from '@/assets/images/illustrationLogin.png';
import { RegexValidation, ValidationOTP } from '@/models/common';
// import { useAppDispatch, useAppSelector } from "@/app/hooks";
// import {
//   selectUser,
//   userConfirmOtp,
//   userSendOtp,
//   userVerifyAccount,
// } from "@/features/user-slice";
// import {
//   IConfirmOtpPayload,
//   IUserSendOtpPayload,
//   IUserSendOtpResponse,
//   IVerifyAccountPayload,
// } from "@/models/user";
// import {
//   MessageErrorConfirmOTP,
//   MessageErrorCreateEmployee,
//   RegexValidation,
//   ResponseErrorConfirmOTP,
//   ValidationOTP,
// } from "@/models/common";
const { Title } = Typography;
type Props = {};
const SendOtp = (props: Props) => {
  let countDountS = 61;
  const router = useRouter();
  const [initSecond, setInitSecond] = useState<number>(0);
  const [disableConfirm, setDisableConfirm] = useState<boolean>(true);
  // const { token, sendOtp, isForgotPassWord, loading, loadingSendOtp } = useAppSelector(selectUser);
  const [otp, setOtp] = useState<number>(0);
  // const dispatch = useAppDispatch();
  const onchangeInput = (value: any) => {
    setOtp(value.target.value);
  };
  const cssButtonGetOTP = [
    {
      background: '#5D5FEF',
      color: 'white',
    },
    {
      color: '#FFFFFF !important',
      background: 'rgba(93, 95, 239, 0.5)',
    },
    {
      color: '#FFFFFF',
      background: 'rgba(24, 144, 255, 0.25)',
    },
    {
      color: '#FFFFFF !important',
      background: 'rgba(93, 95, 239, 0.5) !important',
      fontWeight: 400,
    },
  ];
  // get otp
  const handleGetOTP = () => {
    // let payload: IUserSendOtpResponse = {
    //   Token: token.Payload?.Token,
    // };
    // dispatch(userSendOtp(payload))
    //   .unwrap()
    //   .then()
    //   .then((res: any) => {
    //     const intervalId = setInterval(() => {
    //       countDountS = countDountS - 1;
    //       setInitSecond(countDountS);
    //       if (!countDountS) {
    //         clearInterval(intervalId);
    //         countDountS = 61;
    //       }
    //     }, 1000);
    //   });
  };

  // Confirm OTP
  const handleConfirmOTP = () => {
    // let payload: IConfirmOtpPayload = {
    //   Token: sendOtp.Payload.Token,
    //   Receiver: String(router.query.user),
    //   Otp: otp,
    // };
    // dispatch(userConfirmOtp(payload))
    //   .unwrap()
    //   .then()
    //   .then((res: any) => {
    //     if (!isForgotPassWord) {
    //       let payload: IVerifyAccountPayload = {
    //         Token: res.Payload?.Token,
    //       };
    //       dispatch(userVerifyAccount(payload))
    //         .unwrap()
    //         .then()
    //         .then((res: any) => {
    //           message.success({
    //             content: MessageErrorCreateEmployee.SUCCESSREGISTER,
    //             className: "custom-class",
    //             style: {
    //               marginTop: "3vh",
    //             },
    //           });
    //           window.location.href = "/auth";
    //         });
    //     } else {
    //       router.push("/auth/new-password");
    //     }
    //   })
    //   .catch((error: any) => {
    //     if (error.Payload?.TranslateKey == ResponseErrorConfirmOTP.NOTMATCH) {
    //       message.error({
    //         content: MessageErrorConfirmOTP.NOTMATCH,
    //         className: "erroNotFound-class",
    //         style: {
    //           marginTop: "3vh",
    //         },
    //       });
    //     }
    //   });
  };
  return (
    <Fragment>
      <Head>
        <title>{settings?.title_login}</title>
      </Head>
      <WrapperAuth>
        <WrapperLogin>
          <LogoLogin>
            <Image src={ImageLogoLogin} alt="Logo login" />
          </LogoLogin>
          <SendOtpWrapper>
            <SendOtpTop>
              <Title level={3}>Xác nhận thông tin</Title>
              <SendOtpSpan>
                Vui lòng nhập mã OTP chúng tôi đã gửi đến bạn qua Email/ Số điện thoại đăng ký.
              </SendOtpSpan>
            </SendOtpTop>
            <Form>
              <Form.Item
                name={'otp'}
                rules={[
                  {
                    required: true,
                    message: ValidationOTP.OTPEMPTY,
                  },
                  {
                    validator: async (rule: any, value: any, callback) => {
                      if (isNaN(value) || RegexValidation.REGEXOTP.test(value)) {
                        setDisableConfirm(true);
                        return Promise.reject(ValidationOTP.ERROROTP);
                      } else {
                        setDisableConfirm(false);
                      }
                    },
                  },
                ]}
              >
                <InputOtpWrapper>
                  <InputOtp
                    size="large"
                    maxLength={6}
                    minLength={6}
                    onChange={onchangeInput}
                    placeholder={ValidationOTP.CODEOTP}
                    prefix={<TabletOutlined />}
                  />
                  <InputOtpBtn
                    size={'large'}
                    disabled={initSecond > 0}
                    style={initSecond > 0 ? cssButtonGetOTP[1] : cssButtonGetOTP[0]}
                    onClick={handleGetOTP}
                    // loading={loadingSendOtp}
                  >
                    Lấy mã {initSecond == 0 ? '' : '(' + initSecond}
                    {initSecond == 0 ? '' : 's)'}
                  </InputOtpBtn>
                </InputOtpWrapper>
              </Form.Item>
            </Form>
            <ButtonNext
              type="primary"
              size={'large'}
              onClick={handleConfirmOTP}
              disabled={disableConfirm}
              // loading={loading}
              style={disableConfirm ? cssButtonGetOTP[2] : {}}
            >
              Xác nhận
            </ButtonNext>
            <p
              onClick={() => router.back()}
              style={{ textAlign: 'center', cursor: 'pointer', fontSize: '14px' }}
            >
              Quay lại
            </p>
          </SendOtpWrapper>
        </WrapperLogin>
      </WrapperAuth>
    </Fragment>
  );
};
SendOtp.Layout = AuthLayout;
export default SendOtp;
