const config = {
  baseUrl: process.env.BASE_URL,
  environment: process.env.ENVIRONMENT,
};
export default config;
