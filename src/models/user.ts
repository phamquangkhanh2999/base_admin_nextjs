export interface IPayloadLogin {
  email: string;
  password: string;
}

export interface IInitState {
  register: any;
  loading: boolean;
  loadingGetListUser: boolean;
  listDataUsers: IResponseGetListUser;
  error: boolean;
  isAuthentication: boolean;
  loadingSendOtp: boolean;
  isForgotPassWord: boolean;
  sendOtp: any;
  otp: any;
  token: any;
  loginInfo: ILoginResponse;
}

export interface IResponseGetListUser {
  TotalCount: number;
  StatusCode: number;
  Payload: IPayloadGetListUser[];
}

export interface IPayloadGetListUser {
  UserName: string;
  Email: string;
  PhoneNumber: number;
  NormalizedUserName: string;
  NormalizedEmail: string;
  Roles: any;
  Lockout: boolean;
  LockoutEnd: string;
  IsActive: boolean;
  Id: string;
  CreatedTime: string;
  CreatedBy: string;
  ModifiedTime: string;
  ModifiedBy: string;
  IsDeleted: boolean;
  DeletedBy: string;
  DeletedTime: string;
}

export interface IPayloadRefresToken {
  RefreshToken: string;
  AccessSystem: string;
}

export interface IRefreshTokenResponse {
  AccessToken: string;
  RefreshToken: string;
  RefreshTokenExpired: Date;
  AccessTokenExpired: Date;
  Id: string;
}

export interface ILoginResponse {
  StatusCode: string;
  Payload: {
    Id: string;
    UserName: string;
    Email: string;
    PhoneNumber: string;
    AccessToken: string;
    AccessTokenExpired: string;
    RefreshToken: string;
    RefreshTokenExpired: string;
    Roles: [];
    UserId: string;
  };
  Message: string;
  Type: string;
}
export interface IChangePasswordPayload {
  CurrentPassword: string;
  NewPassword: string;
}

export interface ILogoutPayload {
  RefreshToken: string;
  AccessSystem: string;
}

export interface IAvatar {
  Id: string;
  AbsoluteUri: string;
  RelativeUri: string;
  AbsolutePath: string;
  RelativePath: string;
  FileName: string;
  OriginalFileName: string;
  ContentLength: 0;
  ThumbnailAbsoluteUri: null;
  ThumbnailRelativeUri: null;
  ThumbnailPath: string;
  Mime: string;
}
export interface IUpdateProfilePayload {
  Id: string;
  DisplayName: string;
  Email: string;
  Gender: number;
  PhoneNumber: string;
  Birthday: string;
  Identification: any;
  IsPrimary: boolean;
  Avatar?: IAvatar | null;
}
export interface IUserResponse {
  DisplayName: string;
  Email: string;
  Gender: string;
  PhoneNumber: string;
  Birthday: any;
}

export interface IUpdateProfilePayload {
  Id: string;
  DisplayName: string;
  Email: string;
  Gender: number;
  PhoneNumber: string;
  Birthday: string;
  Identification: any;
  IsPrimary: boolean;
  Avatar?: IAvatar | null;
}
export interface IForgotPasswordPayload {
  User: string;
}
export interface ILoginResponseNotActive {
  Token: string;
  TranslateContext: string;
  TranslateKey: string;
}
export interface IUserSendOtpPayload {
  Token: string;
}
export interface IUserSendOtpResponse {
  Token: string;
}

export interface IConfirmOtpPayload {
  Token: string;
  Receiver: string;
  Otp: number;
}
export interface IVerifyAccountPayload {
  Token: string;
}
export interface IConfirmOtpPayload {
  Token: string;
  Receiver: string;
  Otp: number;
}

export interface IRegisterResponse {
  VerificationRequirement: boolean;
  Token: string;
  // profileCollection:IProfileCollection[]
}
export interface IResetPasswordPayload {
  Token: string;
  NewPassword: string;
}
