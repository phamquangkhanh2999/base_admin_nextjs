import { IPagination } from '@/models/common';
import moment from 'moment';

export const commons = {
  generateParams: (pagination: IPagination, filterData: any, order: any) => {
    let payload: any = {};
    const { page, perPage } = pagination;
    const top = perPage;
    const skip = perPage * (page - 1);
    payload.$top = top;
    payload.$skip = skip;
    if (order !== null) {
      if (order === 'asc') {
        payload.$orderby = `CreatedTime asc`;
      } else {
        payload.$orderby = `CreatedTime desc`;
      }
    }
    if (filterData && Object.keys(filterData).length > 0) {
      let filter: any = [];
      let filters = '';
      Object.keys(filterData).map((key) => {
        let value = filterData[key];
        if (key === 'RequestType' || key === 'Status' || key === 'IsDeleted') {
          if (key === 'IsDeleted') {
            if (value && value !== '') filter.push(`${key} eq ${value}`);
          } else {
            if (value && value !== '') filter.push(`${key} eq '${value}'`);
          }
        } else if (key === '$orderby') {
          if (value && value !== '') filter.push(`${key} = '${value}'`);
        } else if (key === 'Roles') {
          filter.push(`Roles/any(d:contains(d, '${value}'))`);
        } else if (key === 'idCatalog') {
          filter.push(`Id ne ${value[0]} and Id ne ${value[1]}`);
        } else if (key === 'Catalogs') {
          filter.push(`Catalogs/any(Catalogs:Catalogs eq ${value})`);
        } else if (key === 'rangeDate') {
          const startDate = moment(value[0]).format('YYYY-MM-DD');
          const endDate = moment(value[1]).format('YYYY-MM-DD');
          filter.push(`StartDate ge ${startDate} and EndDate le  ${endDate}`);
        } else {
          if (value && value !== '') filter.push(`contains(${key}, '${value}')`);
        }
      });
      filters = filter.join(' and ');
      if (filter.length > 0) payload.$filter = `${filters}`;
    }
    return payload;
  },
};
