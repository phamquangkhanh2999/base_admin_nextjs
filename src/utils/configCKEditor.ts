export const editorConfig = {
  toolbar: [
    'heading',
    '|',
    'bold',
    'italic',
    '|',
    'bulletedList',
    'numberedList',
    '|',
    'imageUpload',
    'insertTable',
    '|',
    'alignment:left',
    'alignment:right',
    'alignment:center',
    'alignment:justify',
    'link',
    'blockQuote',
    '|',
    'undo',
    'redo',
    'highlight',
  ],
  heading: {
    options: [
      { model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph' },
      {
        model: 'heading1',
        view: 'h1',
        title: 'Heading 1',
        class: 'ck-heading_heading1',
      },
      {
        model: 'heading2',
        view: 'h2',
        title: 'Heading 2',
        class: 'ck-heading_heading2',
      },
    ],
  },
  ckfinder: {
    uploadUrl: 'https://zuka-api.dpotech.vn/msa-storage/odata/storage/upload',
  },
  table: {
    contentToolbar: ['tableColumn', 'tableRow', 'mergeTableCells'],
  },

  image: {
    toolbar: [
      'imageTextAlternative',
      '|',
      'imageStyle:alignLeft',
      'imageStyle:full',
      'imageStyle:alignRight',
    ],
    resizeUnit: '%',
    styles: [
      // This option is equal to a situation where no style is applied.
      'full',
      // This represents an image aligned to the left.
      'alignLeft',
      // This represents an image aligned to the right.
      'alignRight',
    ],
  },
  alignment: {
    options: ['left', 'right', 'center', 'justify'],
  },
};
