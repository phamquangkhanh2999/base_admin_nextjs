import React from 'react';
import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { editorConfig } from '@/utils/configCKEditor';
import MyUploadAdapter from './my-upload-adapter';

const _init = (editor: any) => {
  editor.editing.view.change((writer: any) => {
    writer.setStyle('min-height', '200px', editor.editing.view.document.getRoot());
  });
  editor.plugins.get('FileRepository').createUploadAdapter = (loader: any) => {
    return new MyUploadAdapter(loader);
  };
};

interface ICkeditor {
  data: string;
  setEditor: React.Dispatch<React.SetStateAction<string>>;
}
const Ckeditor = (props: ICkeditor) => {
  return (
    <div>
      {' '}
      <CKEditor
        editor={ClassicEditor}
        data={props.data}
        onReady={_init}
        config={editorConfig}
        onChange={(event: any, editor: any) => {
          let data = editor.getData();
          props.setEditor(data);
        }}
      />
    </div>
  );
};

export default Ckeditor;
