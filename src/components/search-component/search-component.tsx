import React, { ReactNode } from 'react';
import { SearchFormWrapper } from './search-component.styled';
import { Col, Row, Space } from 'antd';
import { ButtonPrimary } from '../styled-components/common-styled';
import { ReloadOutlined, SearchOutlined } from '@ant-design/icons';

interface ISearchComponent {
  onHandleReload: () => void;
  onHandleSearch: () => void;
  colProps: number;
  children: ReactNode;
}
const SearchComponent = (props: ISearchComponent) => {
  const { onHandleSearch, onHandleReload, children, colProps } = props;
  return (
    <SearchFormWrapper>
      <Row gutter={[16, 16]}>
        {children}
        <Col xs={colProps} xxl={colProps} xl={colProps} sm={24}>
          <Space className="searchEmployees">
            <ButtonPrimary
              size="large"
              type="primary"
              icon={<SearchOutlined className="icon-search" />}
              onClick={onHandleSearch}
            >
              Tìm kiếm
            </ButtonPrimary>
            <ReloadOutlined onClick={onHandleReload} className="icon-reload" />
          </Space>
        </Col>
      </Row>
    </SearchFormWrapper>
  );
};

export default SearchComponent;
