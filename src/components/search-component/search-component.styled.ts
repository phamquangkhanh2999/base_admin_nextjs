import styled from 'styled-components';

export const SearchFormWrapper = styled.div`
  margin-bottom: 30px;
  background: #ffffff;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
  border-radius: ${({ theme }) => theme.borderRadius.radiusDefault};
  padding: 30px;

  .icon-reload {
    font-size: 20px;
    color: #4079a3;
    cursor: pointer;
  }
  .icon-search {
    font-weight: 800;
  }
  .ant-input-affix-wrapper-lg {
    border-color: #4079a3 !important;
  }
`;
