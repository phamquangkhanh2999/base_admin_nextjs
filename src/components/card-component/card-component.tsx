import React, { ReactNode } from 'react';
import { CardWrapper, CartContent, CartTitle, CartTitleText } from './card-component.styled';

interface ICardComponent {
  children: ReactNode;
  cartTitle?: string;
}
const CardComponent = (props: ICardComponent) => {
  const { children, cartTitle } = props;
  return (
    <CardWrapper>
      {cartTitle && (
        <CartTitle>
          <CartTitleText>{cartTitle}</CartTitleText>
        </CartTitle>
      )}
      <CartContent>{children}</CartContent>
    </CardWrapper>
  );
};

export default CardComponent;
