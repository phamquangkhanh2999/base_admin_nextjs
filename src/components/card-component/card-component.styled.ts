import styled from 'styled-components';

export const CardWrapper = styled.div`
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
  border-width: 0;
  transition: all 0.2s;
  background-color: #fff;
  margin-bottom: 28px;
  border-radius: ${({ theme }) => theme.borderRadius.radiusDefault};
`;

export const CartTitle = styled.div`
  display: flex;
  align-items: center;
  padding: 15px 30px 15px;
  border-bottom: 1px solid #4079a3;
`;

export const CartTitleText = styled.span`
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 700;
  font-size: 14px;
  line-height: 19px;
  color: #4079a3;
`;
export const CartContent = styled.div`
  padding: 15px 30px 25px;
  .datePicker {
    width: 100%;
    border-color: #4079a3;
  }
`;
