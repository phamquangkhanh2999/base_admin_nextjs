import { Pagination } from 'antd';
import React from 'react';
interface IPaginationComponent {
  current: number;
  pageSize: number;
  onChangePagination: any;
  totalCount: number;
}
const PaginationComponent = (props: IPaginationComponent) => {
  const { onChangePagination, current, pageSize, totalCount } = props;
  const handleChange = (page: number, pageSize: number) => {
    onChangePagination({ page: page, perPage: pageSize });
  };
  return (
    <Pagination
      size="small"
      current={current}
      pageSize={pageSize}
      total={totalCount}
      showTotal={(total) => `${total} bản ghi`}
      onChange={handleChange}
      showSizeChanger
    />
  );
};

export default PaginationComponent;
