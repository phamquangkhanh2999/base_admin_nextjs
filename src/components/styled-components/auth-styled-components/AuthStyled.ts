import { Button, Input } from 'antd';
import styled, { css } from 'styled-components';
import LogoAuth from '@/assets/images/image_auth.jpg';

export const WrapperAuth = styled.div`
  display: flex;
  align-items: center;
  height: 100vh;
`;
export const AuthLeft = styled.div`
  height: 100vh;
  object-fit: cover;
  flex: 0 0 33.33333%;
  max-width: 33.33333%;
  background: url(${() => LogoAuth.src}) no-repeat center;
  background-size: contain;
  background-color: white;
  ${({ theme }) => theme.mediaQueries.tablet} {
    display: none;
  }
  ${({ theme }) => theme.mediaQueries.mobile} {
    display: none;
  }
`;
export const AuthRight = styled.div`
  flex: 0 0 66.66667%;
  max-width: 66.66667%;
  ${({ theme }) => theme.mediaQueries.tablet} {
    flex: 0 0 100%;
    max-width: 100%;
  }
  ${({ theme }) => theme.mediaQueries.mobile} {
    flex: 0 0 100%;
    max-width: 100%;
  }
`;

export const AuthContent = styled.div`
  padding: 0px 80px;
  margin: auto;
  ${({ theme }) => theme.mediaQueries.tablet} {
    padding: 0px 20px;
  }
  ${({ theme }) => theme.mediaQueries.mobile} {
    padding: 0px 20px;
  }
`;
export const WrapperLogin = styled.div`
  position: relative;
  /* box-shadow: 0 2px 8px rgba(0, 0, 0, 0.15); */
  box-shadow: rgba(50, 50, 93, 0.25) 0px 50px 100px -20px, rgba(0, 0, 0, 0.3) 0px 30px 60px -30px,
    rgba(10, 37, 64, 0.35) 0px -2px 6px 0px inset;
  background-color: #ffff;
  border-radius: 14px;
  padding: 27px 48px;
  width: 580px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  flex-direction: row-reverse;
  ${({ theme }) => theme.mediaQueries.mobile} {
    max-width: 400px;
    flex-wrap: wrap;
    justify-content: center;
  }
`;
export const LogoLogin = styled.div`
  /* ${({ theme }) => theme.mediaQueries.mobile} {
    display: none;
  } */
`;

// styled Forgot password
export const WrapperForgot = styled.div`
  display: flex;
  flex-direction: column;
`;
export const InputEmail = styled(Input)`
  margin-top: 38px;
  width: 100%;
  border-radius: 10px;
  .ant-input-prefix {
    color: #1890ff;
    padding-right: 7px;
  }
`;

export const ButtonNext = styled(Button)`
  width: 100%;
  margin-top: 38px;
  margin-bottom: 24px;
  border-radius: 10px;
`;

// styled Send OTP

export const SendOtpWrapper = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
`;
export const SendOtpTop = styled.div``;

export const SendOtpSpan = styled.div`
  color: rgba(0, 0, 0, 0.45);
  font-size: 14px;
  text-align: center;
`;

export const InputOtpWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-top: 36px;
  gap: 10px;
`;
export const InputOtp = styled(Input)`
  border-radius: 10px;
  .ant-input-prefix {
    color: #1890ff;
    padding-right: 7px;
  }
`;

export const InputOtpBtn = styled(Button)`
  border-radius: 10px;
  background-color: #5d5fef;
  color: white;
  font-size: 14px;
`;

// styled new password
export const NewPasswordWrapper = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
`;
