import styled from 'styled-components';
import { Button, Table, Typography } from 'antd';

export const HrStyle = styled.hr`
  color: #e9ecef;
  margin-bottom: 20px;
`;
const Text = Typography;

export const FontBold = styled.p`
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 700;
  font-size: 14px;
  line-height: 19px;
  color: #000000;
`;

export const WrapperTable = styled.div`
  display: flex;
  flex-direction: column;
  gap: 40px;
`;
export const PaginationWrapper = styled.div`
  display: flex;
  align-items: flex-end;
  flex-direction: column;
`;
export const TableCustoms = styled(Table)`
  background: #ffffff;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
  border-radius: ${({ theme }) => theme.borderRadius.radiusDefault};
  .ant-table,
  .ant-table-placeholder,
  .ant-table-cell-row-hover {
    background: unset !important;
    border-radius: unset !important;
  }

  .ant-table-cell {
    border-bottom: unset !important;
    font-weight: 400;
    font-size: 14px;
    line-height: 19px;
    background-color: unset !important;
    @media only screen and (max-width: 768px) {
      font-size: 11px;
    }
    &::before {
      content: none !important;
    }
  }
  td.ant-table-cell {
    border-top: 0.5px solid #4079a3 !important;
  }
  .ant-table-thead .ant-table-cell {
    font-family: 'Open Sans';
    font-style: normal;
    font-weight: 800;
    font-size: 14px;
    line-height: 19px;
    color: #000000;
    background-color: ${({ theme }) => theme.colors.backgroundHeadTable} !important;
    @media only screen and (max-width: 768px) {
      font-size: 11px;
    }
  }

  .ant-table-body {
    margin-top: 24px !important;
    .ant-table-row {
      height: 40px;
    }
    &::-webkit-scrollbar {
      width: 12px;
    }

    &::-webkit-scrollbar-track {
      border: 1px solid #83a2bb;
    }

    &::-webkit-scrollbar-thumb {
      background-color: #83a2bb;
    }
  }

  .ant-table-cell-scrollbar {
    box-shadow: unset !important;
  }
  .ant-empty-image {
    display: none !important;
  }
  .ant-empty-description {
    color: #c5cde8 !important;
  }
`;
export const SectionBox = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-end;
  margin-top: 40px;
  gap: 10px;
`;
export const ButtonComeback = styled(Button)`
  border-radius: ${({ theme }) => theme.borderRadius.radiusDefault};
  font-weight: 700;
  font-size: 14px !important;
  line-height: 19px;
  color: #090a0b;
  background: #c4c4c4;
  min-width: 97px;
  &:hover {
    border-color: #090a0b !important;
    color: #090a0b !important;
  }
`;
export const ButtonPrimary = styled(Button)`
  min-width: 97px;
  border-radius: ${({ theme }) => theme.borderRadius.radiusDefault};
  color: #ffffff;
  font-weight: 700;
  font-size: 14px !important;
  line-height: 19px;
  background: ${(props: { bgcolor?: 'danger' | 'success' }) => {
    if (props.bgcolor === 'success') {
      return '#36B843';
    } else if (props.bgcolor === 'danger') {
      return '#F90707';
    } else {
      return '#4079a3';
    }
  }};
  &:hover {
    background-color: ${(props: { bgcolor?: 'danger' | 'success' }) => {
      if (props.bgcolor === 'success') {
        return '#36B843b5';
      } else if (props.bgcolor === 'danger') {
        return '#f90707b5';
      } else {
        return '#4079a3b5';
      }
    }} !important;
  }
`;
