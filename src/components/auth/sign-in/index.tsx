import React, { useState } from 'react';
import { Checkbox, Col, Form, Input, Row, Typography, message } from 'antd';
import { LockOutlined, MailOutlined, UserOutlined } from '@ant-design/icons';
import { useRouter } from 'next/router';
import { IPayloadLogin } from '@/models/user';
import { selectUser, userSignIn } from '@/features/user-slice';
import { useAppDispatch, useAppSelector } from '@/app/hooks';
import {
  ButtonLogin,
  ButtonWrapper,
  FormWrapper,
  SingInText,
  SingInTitle,
  StyledFormLogin,
  WrapperFooter,
} from './SignInStyled';
import { ResponseErrorSignin } from '@/models/common';
import { HrStyle } from '@/components/styled-components/common-styled';
const { Title, Text } = Typography;
type Props = {};

const SignIn = (props: Props) => {
  const [form] = Form.useForm();
  const router = useRouter();
  const [isModalVisible, setIsModalVisible] = useState<boolean>(false);

  const dispatch = useAppDispatch();
  const [checked, setChecked] = useState<boolean>(false);
  const { loading } = useAppSelector(selectUser);
  const [btnDisabled, setBtnDisabled] = useState<boolean>(true);
  const showModalActiveAccount = () => {
    setIsModalVisible(true);
  };

  const onValuesChange = (allValues: any) => {
    if (
      form.getFieldValue('email') === undefined ||
      form.getFieldValue('password') === undefined ||
      form.getFieldValue('email') === '' ||
      form.getFieldValue('password') === ''
    ) {
      setBtnDisabled(true);
    } else {
      setBtnDisabled(false);
    }
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const onCheckboxChange = (e: any) => {
    setChecked(!checked);
  };

  const onFinishFailed = (errorInfo: any) => {};

  const onFinish = (values: any) => {
    let payload: IPayloadLogin = { ...values };
    dispatch(userSignIn(payload))
      .unwrap()
      .then()
      .then((res: any) => {
        message.success('Đăng nhập thành công');
        window.location.href = '/';
      })
      .catch((error: any) => {
        if (error.Payload?.TranslateKey == ResponseErrorSignin.NOTFOUND) {
          message.error('Tài khoản hoặc mật khẩu của bạn không chính xác!');
        } else if (error.Payload?.TranslateKey == ResponseErrorSignin.INCORRECT_PASSWORD) {
          message.error('Mật khẩu bạn vừa nhập không đúng vui lòng thử lại!');
        } else if (error.Payload?.TranslateKey == 'AccountIsLockedException') {
          message.error('Tài khoản của bạn đã bị khóa!');
        } else {
          message.error(error?.Message);
        }
        if (error.Payload?.TranslateKey == ResponseErrorSignin.NOTACTIVE) {
          showModalActiveAccount();
        }
      });
  };

  return (
    <React.Fragment>
      <SingInTitle>
        <Title>Đăng nhập</Title>
        <SingInText>
          <Text>
            Xin chào và chào mừng bạn đến với hệ thống admin của chúng tôi. Để sử dụng các tính năng
            của chúng tôi, vui lòng đăng nhập bằng tài khoản của bạn Hãy nhập thông tin đăng nhập
            của bạn bên dưới để bắt đầu sử dụng hệ thống của chúng tôi.
          </Text>
        </SingInText>
      </SingInTitle>

      <FormWrapper>
        <div className="form-login">
          <div className="login-content">
            <StyledFormLogin
              name="basic"
              // initialValues={{ remember: true }}
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
              onChange={onValuesChange}
              autoComplete="off"
              form={form}
              layout="vertical"
            >
              <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                <Col xxl={12} xl={12} sm={12} xs={24}>
                  <Form.Item
                    name="email"
                    label="Email"
                    rules={[
                      {
                        required: true,
                        message: 'Email không được để trống',
                      },
                    ]}
                  >
                    <WrapperFooter>
                      <Input
                        prefix={<MailOutlined className="site-form-item-icon" />}
                        placeholder="Email"
                        // onChange={onChange}
                        size={'large'}
                        maxLength={100}
                      />
                    </WrapperFooter>
                  </Form.Item>
                </Col>
                <Col xxl={12} xl={12} sm={12} xs={24}>
                  <Form.Item
                    name="password"
                    label="Password"
                    rules={[
                      {
                        required: true,
                        message: 'Mật khẩu không được để trống',
                      },
                    ]}
                  >
                    <Input.Password
                      prefix={<LockOutlined className="site-form-item-icon" />}
                      type="password"
                      placeholder="Mật khẩu"
                      size={'large'}
                      maxLength={20}
                    />
                  </Form.Item>
                </Col>
              </Row>
              <HrStyle color="#e9ecef" />
              <div className="resetPassword">
              </div>
              <ButtonWrapper>
                <ButtonLogin
                  type={'primary'}
                  htmlType={'submit'}
                  size={'large'}
                  disabled={btnDisabled}
                  loading={loading}
                >
                  Đăng nhập
                </ButtonLogin>
              </ButtonWrapper>
            </StyledFormLogin>
          </div>
        </div>
      </FormWrapper>
    </React.Fragment>
  );
};
export default SignIn;
