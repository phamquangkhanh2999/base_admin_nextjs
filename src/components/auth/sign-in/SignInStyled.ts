import styled from 'styled-components';
import { Button, Form } from 'antd';

export const FormWrapper = styled.div``;

export const StyledFormLogin = styled(Form)`
  .ant-form {
    padding: 80px !important;
  }
  .ant-input-affix-wrapper {
    border-radius: 10px;
  }
  .ant-input-prefix {
    color: #1890ff;
    margin-right: 14px;
  }
  .ant-input-password-icon.anticon {
    color: #1890ff;
  }
  .resetPassword {
    display: flex;
    align-items: center;
    justify-content: space-between;
  }
  .rest {
    padding-bottom: 25px;
    cursor: pointer;
    color: #1890ff;
    text-decoration-line: underline;
    line-height: 24px;
    font-size: 16px;
  }
`;
export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
`;
export const ButtonLogin = styled(Button)`
  border-radius: 10px;
  text-align: center;
  font-weight: 500;
  background: ${(props: any) => props.color};
  .ant-btn-primary[disabled] {
    /* background: ${(props: any) => props.disablecolor};  */
    background: 'red';
  }
  border: ${(props) => props.theme.main};
`;
export const SpanFooter = styled.span`
  color: #1890ff;
  text-decoration: underline;
`;
export const WrapperFooter = styled.div`
  align-items: center;
  text-align: center;
`;

export const SingInTitle = styled.div`
  padding-bottom: 20px;
  margin-bottom: 20px;
  border-bottom: 1px solid #e9ecef;
`;
export const SingInText = styled.div`
  max-width: 70%;
  ${({ theme }) => theme.mediaQueries.mobile} {
    max-width: 100%;
  }
`;
