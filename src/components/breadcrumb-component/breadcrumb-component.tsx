import React from 'react';
import { HomeOutlined, PlusOutlined, UserOutlined } from '@ant-design/icons';
import { BreadcrumbStyle, StyleTitleWrapper, TitleStyle } from './breadcrumb-component.styled';
import { Breadcrumb, Button, Space } from 'antd';
import Link from 'next/link';
import { IBreadcrumb } from '@/models/common';
import { ButtonPrimary } from '../styled-components/common-styled';
interface IBreadcrumbComponent {
  data: IBreadcrumb[];
  titlePage?: string;
  textNew?: string;
  visibleButtonNew?: boolean;
  handleCreateNew?: () => void;
}
const BreadcrumbComponent = (props: IBreadcrumbComponent) => {
  const { data, titlePage, handleCreateNew, visibleButtonNew = true, textNew = 'Tạo mới' } = props;
  return (
    <>
      <BreadcrumbStyle>
        <Breadcrumb.Item>
          <Link href="/">Trang Chủ</Link>
        </Breadcrumb.Item>
        {data &&
          data.length > 0 &&
          data.map((item: IBreadcrumb, index: number) => {
            return (
              <Breadcrumb.Item key={index}>
                {item.routeName ? (
                  <Link href={item?.routeName}>{item.name}</Link>
                ) : (
                  <span className={'breadcrumb-item'}>{item.name}</span>
                )}
              </Breadcrumb.Item>
            );
          })}
      </BreadcrumbStyle>
      {titlePage && (
        <StyleTitleWrapper>
          <TitleStyle level={1}>{titlePage}</TitleStyle>
          <Space>
            {visibleButtonNew && (
              <ButtonPrimary
                size="large"
                type="primary"
                icon={<PlusOutlined />}
                onClick={handleCreateNew}
              >
                {textNew}
              </ButtonPrimary>
            )}
          </Space>
        </StyleTitleWrapper>
      )}
    </>
  );
};

export default BreadcrumbComponent;
