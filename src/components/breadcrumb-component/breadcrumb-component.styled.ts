import { Breadcrumb, Button, Typography } from 'antd';
import styled from 'styled-components';

const { Title } = Typography;

export const TitleStyle = styled(Title)`
  margin-bottom: 0.2em !important;
  color: #0f2d59 !important;
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 800 !important;
  font-size: 18px !important;
  line-height: 25px !important;
`;
export const BreadcrumbStyle = styled(Breadcrumb)`
  margin: 10px 0 15px;
  .ant-breadcrumb-link > a {
    font-family: 'Open Sans';
    font-style: normal;
    font-size: 14px;
    line-height: 19px;
    font-weight: 400;
    color: #787474;
  }
  .breadcrumb-item {
    font-family: 'Open Sans';
    font-style: normal;
    font-size: 14px;
    line-height: 19px;
    color: #1890ff;
    font-weight: 400;
  }
`;
export const StyleTitleWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  flex-wrap: wrap;
  margin-bottom: 20px;
`;

export const StyleButtonNew = styled(Button)`
  background: #4079a3;
  border-radius: ${({ theme }) => theme.borderRadius.radiusDefault};
  color: #ffffff;
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 700;
  font-size: 14px;
  line-height: 19px;
`;
