import React from 'react';
import { ButtonComeback, ButtonPrimary, SectionBox } from '../styled-components/common-styled';
import { useRouter } from 'next/router';

interface ICommonButtons {
  loading?: boolean;
  disabled?: boolean;
  bgColor?: 'danger' | 'success';
  titleBtnNew: 'Tạo mới' | 'Cập nhật';
  textBtnStatus?: string | null;
  handleChangeStatus?: () => void;
}
const CommonButtons = (props: ICommonButtons) => {
  const { loading, titleBtnNew, disabled, bgColor, textBtnStatus, handleChangeStatus } = props;
  const router = useRouter();

  return (
    <SectionBox>
      <ButtonComeback size="large" onClick={() => router.back()}>
        Hủy
      </ButtonComeback>
      {textBtnStatus && (
        <ButtonPrimary
          bgcolor={bgColor}
          disabled={disabled}
          size="large"
          type="primary"
          loading={loading}
          onClick={handleChangeStatus}
        >
          {textBtnStatus}
        </ButtonPrimary>
      )}
      <ButtonPrimary
        disabled={disabled}
        size="large"
        htmlType="submit"
        loading={loading}
        type="primary"
      >
        {titleBtnNew}
      </ButtonPrimary>
    </SectionBox>
  );
};

export default CommonButtons;
