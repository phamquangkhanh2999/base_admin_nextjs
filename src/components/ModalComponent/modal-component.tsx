import React, { ReactNode } from 'react';
import {
  FooterModal,
  ModalBtnClose,
  ModalBtnConfirm,
  ModalContainer,
  ModalContent,
  ModalStyle,
  ModalTitle,
} from './modal-component.styled';
import { CloseCircleOutlined } from '@ant-design/icons';

interface IModalComponent {
  isModalOpen: boolean;
  titleHeader: string;
  text: string;
  textBtn?: string;
  isCheckDelete?: boolean;
  loadingBtn?: boolean;
  closeModalOpen: React.Dispatch<React.SetStateAction<boolean>>;
  handleConfirm?: () => void;
}
const ModalComponent = (props: IModalComponent) => {
  const {
    isModalOpen,
    titleHeader,
    textBtn,
    text,
    closeModalOpen,
    isCheckDelete,
    handleConfirm,
    loadingBtn,
  } = props;
  const handleCancel = () => {
    closeModalOpen(false);
  };
  return (
    <ModalStyle closable={false} open={isModalOpen} title={''} footer={false} centered width={332}>
      <ModalContainer>
        <ModalTitle>{titleHeader}</ModalTitle>
        <ModalContent isCheck={isCheckDelete}>{text}</ModalContent>
        <CloseCircleOutlined className="close_circle" onClick={handleCancel} />
        <FooterModal>
          <ModalBtnClose onClick={handleCancel}>Hủy</ModalBtnClose>
          {textBtn && (
            <ModalBtnConfirm type="primary" loading={loadingBtn} onClick={handleConfirm}>
              {textBtn}
            </ModalBtnConfirm>
          )}
        </FooterModal>
      </ModalContainer>
    </ModalStyle>
  );
};

export default ModalComponent;
