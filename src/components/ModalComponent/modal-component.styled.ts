import { Button, Modal } from 'antd';
import backGroundModal from '../../assets/images/background_modal_cuttoms.png';
import styled, { css } from 'styled-components';
// modal
export const ModalStyle = styled(Modal)`
  .note {
    color: red;
    font-size: 13px;
    font-style: italic;
  }
`;
export const ModalContainer = styled.div`
  position: relative;
  .close_circle {
    color: #4079a3;
    position: absolute;
    top: -15px;
    right: -15px;
    font-size: 15px;
  }
`;
export const ModalTitle = styled.div`
  font-style: normal;
  font-weight: 700;
  margin-top: 8px;
  font-size: 16px;
  line-height: 19px;
  text-align: center;
  word-wrap: break-word;
`;
export const ModalMain = styled.div`
  margin-top: 35px;
  margin-bottom: 15px;
  height: 340px;
  max-height: 340px;
  overflow-y: auto;
  @media (max-width: 480px) {
    height: 250px;
    max-height: 250px;
  }
  // css scrollbar
  &::-webkit-scrollbar {
    width: 4px;
    background-color: transparent;
    height: 3px;
  }
  &::-webkit-scrollbar-thumb {
    -webkit-box-shadow: inset 0 0 2px rgb(0 0 0 / 20%);
    background-color: #7538f0;
  }
`;

export const ModalMainTitle = styled.div`
  font-weight: 400;
  font-size: 14px;
  line-height: 120%;
  color: #c5cde8;
  margin-bottom: 8px;
`;
export const ModalContent = styled.div`
  font-family: 'Inter';
  margin: 40px 0px 40px;
  font-style: normal;
  font-weight: 400;
  font-size: 13px;
  line-height: 16px;
  text-align: center;
  ${(props: { isCheck?: boolean }) =>
    props.isCheck &&
    css`
      color: red;
    `}
`;

export const FooterModal = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  gap: 20px;
`;
export const ModalBtnClose = styled(Button)`
  background: #e0dfdf;
  min-width: 73px;
  border-radius: ${({ theme }) => theme.borderRadius.radiusDefault};
  font-weight: 400;
  font-size: 13px !important;
  line-height: 16px;
  color: #000000 !important;
`;
export const ModalBtnConfirm = styled(Button)`
  background: #4079a3 !important;
  min-width: 73px;
  border-radius: ${({ theme }) => theme.borderRadius.radiusDefault};
  font-weight: 400;
  font-size: 13px !important;
  line-height: 16px;
  background: #c4c4c4;
  color: #ffffff;
`;
