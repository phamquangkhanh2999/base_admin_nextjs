import styled from 'styled-components';
import { Layout } from 'antd';
const { Content } = Layout;

export const MainWrapper = styled(Layout)`
  background-color: ${({ theme }) => theme.colors.background};
  .site-layout {
  }
`;
export const MainLayOut = styled.div`
  /* margin: 25px 25px 34px 25px;
  position: relative;
  top: 59px; */
  background-color: ${({ theme }) => theme.colors.background};
  /* min-height: calc(100vh - 124px); */
  /* display: block; */
`;
export const MainLayOutFooter = styled.div`
  color: rgba(0, 0, 0, 0.45);
  margin-top: 20px;
  margin-right: 24px;
  padding-bottom: 24px;
  text-align: right;
  background-color: ${({ theme }) => theme.colors.background};
`;

export const LogoWebWrapper = styled.div`
  height: 32px;
  margin: 16px;

  img {
    width: 100%;
    height: 100%;
  }
`;
export const LayoutContent = styled(Content)`
  padding: 79px 30px 30px 252px;
  /* min-height: calc(100vh - 70px) !important; */
  min-height: 100vh !important;
  background-color: #fff;
  @media screen and (max-width: 992px) {
    padding-left: 24px;
  }
`;
