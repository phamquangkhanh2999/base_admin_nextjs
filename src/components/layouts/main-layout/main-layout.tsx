import React, { useEffect } from 'react';
// import { Header } from "../../common/header";
// import MainHeader from "../../common/header/header";
// import HomePage from '@/pages/home-page';
import moment from 'moment';
import { useRouter } from 'next/router';
import AuthLayout from '../auth-layout/auth-layout';
// import MainSidebar from "./main-sidebar/main-sidebar";
import {
  LayoutContent,
  LogoWebWrapper,
  MainLayOut,
  MainLayOutFooter,
  MainWrapper,
} from './MainLayoutStyled';
import { selectUser } from '@/features/user-slice';
import { LayoutProps } from '@/models/common';
import { useAppSelector } from '@/app/hooks';
import { Layout, Menu } from 'antd';
import { UploadOutlined, UserOutlined, VideoCameraOutlined } from '@ant-design/icons';
import LogoWeb from '@/assets/images/illustrationLogin.png';
import Image from 'next/image';
import MainSidebar from './main-sidebar/main-sidebar';
import { Header } from './header';

const { Content, Footer, Sider } = Layout;
export interface IMainLayout {}

export function MainLayoutHome({ children }: LayoutProps) {
  const router = useRouter();
  const { isAuthentication, loginInfo } = useAppSelector(selectUser);
  // login đăng nhập
  // useEffect(() => {
  //   if (!isAuthentication || moment(loginInfo.Payload?.AccessTokenExpired) < moment())
  //     router.push('/auth');
  // }, [router, isAuthentication, loginInfo]);
  return (
    <>
      {/* {!isAuthentication ? (
        ''
      ) : (
        <MainWrapper>
          <Header />
          <MainSidebar />
          <MainWrapper className="site-layout">
            <MainLayOut>{children}</MainLayOut> 
            <MainLayOutFooter>2021 All rights reserved by DpoTech</MainLayOutFooter>
          </MainWrapper>
        </MainWrapper>
      )} */}
      <Layout>
        <MainSidebar />
        <Layout>
          <Header />
          <LayoutContent>
            <MainLayOut>{children}</MainLayOut>
          </LayoutContent>
          {/* <Footer style={{ textAlign: 'center', backgroundColor: 'white' }}>
            Web admin ©2023 Created by QK
          </Footer> */}
        </Layout>
      </Layout>
    </>
  );
}
