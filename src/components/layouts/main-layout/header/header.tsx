import { Avatar, Badge, Dropdown, Menu, Space, Typography } from 'antd';
import React, { useEffect, useRef, useState } from 'react';
import { useRouter } from 'next/router';
import { Select } from 'antd';
import {
  HeaderWrapper,
  HeaderTitle,
  LogoWrapper,
  NotificationMenu,
  MenuAvatarWrapper,
  MenuAvatarText,
  UserMenu,
  IconSettingCustom,
  BoxUserMenu,
  WrappBoxAvatar,
  ButtonInfomation,
  ButtonLogOut,
  BoxFooter,
  TypographyName,
  StyledEnglish,
  StyledEnglishText,
  LogoWrapperText,
} from './MainHeaderStyled';
import { BellOutlined, GlobalOutlined, LogoutOutlined, UserOutlined } from '@ant-design/icons';
import { useAppDispatch, useAppSelector } from '@/app/hooks';
import { getUserbyID, selectUser, userLogout } from '@/features/user-slice';
import { ILogoutPayload } from '@/models/user';
import type { MenuProps } from 'antd';
interface IMainHeaderProps {}
const { Option } = Select;

const MainHeader: React.FC<IMainHeaderProps> = (props: IMainHeaderProps) => {
  const router = useRouter();
  const [data, setData] = useState<any>();
  const [openUserMenu, setOpenUserMenu] = useState<boolean>(false);
  const wrapperRef = useRef(null);
  const { loginInfo, loading } = useAppSelector(selectUser);
  const handleOpenUserMenu = () => {
    setOpenUserMenu(!openUserMenu);
  };
  const dispatch = useAppDispatch();

  const handleLogout = () => {
    let payload: ILogoutPayload = {
      AccessSystem: 'Mergiservice',
      RefreshToken: loginInfo.Payload.RefreshToken,
    };
    dispatch(userLogout(payload))
      .unwrap()
      .then()
      .then((res: any) => {
        router.push('/auth');
      });
  };

  const useOutsideAlerter = (ref: any) => {
    useEffect(() => {
      function handleClickOutside(event: any) {
        if (ref.current && !ref.current.contains(event.target)) {
          setOpenUserMenu(false);
        }
      }
      document.addEventListener('mousedown', handleClickOutside);
      return () => {
        document.removeEventListener('mousedown', handleClickOutside);
      };
    }, [ref]);
  };

  useEffect(() => {
    dispatch(getUserbyID())
      .unwrap()
      .then()
      .then((res: any) => {
        let payload = { ...res.Payload };
        setData(payload);
      })
      .catch((error: any) => {});
  }, []);

  useOutsideAlerter(wrapperRef);

  const menuItem: MenuProps['items'] = [
    {
      key: '1',
      label: (
        <Space>
          <LogoutOutlined />
          Đăng xuất
        </Space>
      ),
    },
  ];
  return (
    <HeaderWrapper className="site-layout-background" style={{ padding: 0 }}>
      <LogoWrapper>
        <LogoWrapperText>Kidden Garden</LogoWrapperText>
      </LogoWrapper>
      <NotificationMenu>
        <Badge dot>
          <BellOutlined style={{ height: '20x', width: '20px', color: 'white' }} />
        </Badge>
        {/* <BellOutlined /> */}
        <Dropdown menu={{ items: menuItem }} trigger={['click']}>
          <UserMenu>
            <Avatar
              size={30}
              icon={<UserOutlined />}
              style={{ marginRight: '10px' }}
              src={data?.Avatar?.AbsoluteUri}
            />
            <TypographyName>Diệp Phàm</TypographyName>
            <MenuAvatarText style={{ marginRight: '10px' }}>{data?.DisplayName}</MenuAvatarText>
          </UserMenu>
        </Dropdown>
        <StyledEnglish>
          <GlobalOutlined />
          <StyledEnglishText>VN</StyledEnglishText>
        </StyledEnglish>

        {/* <IconSettingCustom /> */}
      </NotificationMenu>
      {openUserMenu ? (
        <BoxUserMenu ref={wrapperRef}>
          <WrappBoxAvatar>
            <Avatar
              size={100}
              icon={<UserOutlined />}
              style={{ marginRight: '10px' }}
              src={data?.Avatar?.AbsoluteUri}
            />
          </WrappBoxAvatar>
          {/* <div className='name'>{data?.DisplayName}</div> */}
          <div
            style={{
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
              marginBottom: '12px',
              marginTop: '12px',
            }}
          >
            <ButtonInfomation onClick={() => router.push('/auth/info-account/[new]')}>
              Thông tin tài khoản
            </ButtonInfomation>
          </div>
          <hr />
          <div
            style={{
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: '12px',
            }}
          >
            <ButtonLogOut onClick={handleLogout} loading={loading}>
              Đăng xuất
            </ButtonLogOut>
          </div>
          <BoxFooter>
            <div>Chính sách bảo mật</div>
            <div>Điều khoản dịch vụ</div>
          </BoxFooter>
        </BoxUserMenu>
      ) : null}
    </HeaderWrapper>
  );
};

export default MainHeader;
