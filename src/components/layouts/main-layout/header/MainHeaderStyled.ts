import styled from 'styled-components';
import { Button, Layout, Menu, Typography } from 'antd';
const { Header } = Layout;
const { Text } = Typography;

import {
  UserOutlined,
  BellOutlined,
  GlobalOutlined,
  GroupOutlined,
  SettingFilled,
  ReloadOutlined,
} from '@ant-design/icons';

export const HeaderWrapper = styled(Header)`
  /* background-color: ${({ theme }) => theme.colors.backgroundMain}; */
  /* background-color: #f4f4f5 !important;
  box-shadow: 0px 4px 25px rgba(61, 17, 189, 0.08); */
  display: flex;
  position: fixed;
  top: 0;
  right: 0;
  height: 64px;
  width: 100%;
  justify-content: space-between;
  align-items: center;
  padding: 0px 5px 0 0;
  z-index: 10;
  color: #ffffff !important;
  background: ${({ theme }) => theme.colors.colorBlue700} !important;
  /* box-shadow: 0px 2px 2px rgba(163, 171, 185, 0.26); */
`;
export const LogoWrapper = styled.div`
  margin: 16px 24px;
`;
export const LogoWrapperText = styled.p`
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 800;
  font-size: 20px;
  line-height: 27px;
`;
export const HeaderTitle = styled.div`
  color: #fff;
  font-weight: 700;
  font-size: 20px;
  font-family: 'Roboto';
  /* padding: 20px 20px 0 20px ; */
  padding: 20px 20px 0 0px;
  margin-bottom: 20px;
  cursor: pointer;
`;

export const NotificationMenu = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  padding-right: 30px;
`;
export const MenuAvatarWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;
export const MenuAvatarText = styled.div`
  padding: 0px;
  /* color: #ffff; */
  padding-right: 7px;
`;
export const BoxUserMenu = styled.div`
  width: 380px;
  height: auto;
  position: fixed;
  background: #ffffff;
  box-shadow: 0px 3px 6px -4px rgba(0, 0, 0, 0.12), 0px 6px 16px rgba(0, 0, 0, 0.08),
    0px 9px 28px 8px rgba(0, 0, 0, 0.05);
  border-radius: 4px;
  top: 58px;
  right: 12%;
  z-index: 100;
  font-family: 'Roboto';
  font-style: normal;
  font-weight: 400;
  color: #14142b;
  font-size: 14px;
  .name {
    text-align: center;
    /* margin: 12px 0 12px 0; */
  }
`;
export const BoxUserMenuBussiness = styled.div`
  position: fixed;
  width: 380px;
  height: 300px;
  background: #ffffff;
  box-shadow: 0px 3px 6px -4px rgba(0, 0, 0, 0.12), 0px 6px 16px rgba(0, 0, 0, 0.08),
    0px 9px 28px 8px rgba(0, 0, 0, 0.05);
  border-radius: 4px;
  top: 58px;
  right: 12%;
  z-index: 100;
  font-family: 'Roboto';
  font-style: normal;
  font-weight: 400;
  color: #14142b;
  font-size: 14px;
  .name {
    text-align: center;
    margin: 12px 0 12px 0;
  }
`;
export const WrappBoxAvatar = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: 24px;
`;
export const ButtonInfomation = styled(Button)`
  width: 52.8%;
  height: 32px;
  background: #1890ff;
  border: 1px solid #1890ff;
  border-radius: 4px;
  color: #ffffff;
`;
export const ButtonLogOut = styled(Button)`
  width: 27.4%;
  height: 32px;
  background: #1890ff;
  border: 1px solid #1890ff;
  border-radius: 4px;
  color: #ffffff;
`;
export const BoxFooter = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  div {
    padding: 24px;
  }
`;
export const WrappContentCompany = styled.div`
  width: 75%;
  height: 42px;
  display: flex;
  align-items: center;
  div {
    cursor: pointer;
    &:hover {
      color: #1890ff;
    }
  }
`;
export const WrapperCompany = styled.div`
  height: 42px;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;
export const BoxAvatar = styled.div`
  width: 100px;
  height: 100px;
  background-color: rebeccapurple;
  border-radius: 50%;
  overflow: hidden;
`;
export const GroupOutlinedCustom = styled(GroupOutlined)`
  width: 16px;
  height: 16px;
  margin-right: 10px;
`;
export const ReloadOutlinedCustom = styled(ReloadOutlined)`
  width: 16px;
  height: 16px;
  margin-right: 10px;
`;
export const IconSettingCustom = styled(SettingFilled)`
  color: #ffffff;
`;
export const UserMenu = styled.div`
  display: flex;
  align-items: center;
  margin-left: 20px;
  cursor: pointer;
  color: ${(props) => props.theme.solidColor} !important;
  p {
    margin-bottom: 0;
    margin-right: 8px;
    color: ${(props) => props.theme.solidColor} !important;
  }
  .svg {
    color: ${(props) => props.theme.solidColor} !important;
  }
`;
export const AvatarWrapper = styled.div`
  width: 40px;
  height: 40px;
  overflow: hidden;
  border-radius: 50%;
  margin-right: 8px;
`;

export const MenuStyled = styled(Menu)`
  li.ant-dropdown-menu-item.ant-dropdown-menu-item-active.ant-dropdown-menu-item-only-child,
  .ant-dropdown-menu-item-selected,
  .ant-dropdown-menu-submenu-title-selected {
    background-color: ${({ theme }) => theme.colors.backgroundActive};
    color: ${({ theme }) => theme.colors.colorText};
  }
`;

export const TypographyName = styled(Text)`
  font-family: 'Roboto', sans-serif;
  color: #ffffff;
  font-weight: 400;
  font-size: 14px;
  line-height: 22px;
  font-style: normal;
`;

export const StyledEnglish = styled.div`
  display: flex;
  align-items: center;
  gap: 10px;
`;
export const StyledEnglishText = styled.div`
  font-family: 'Roboto';
  font-style: normal;
  font-weight: 700;
  font-size: 14px;
  line-height: 22px;
  color: #ffffff;
`;
