import React, { useEffect, useState } from 'react';
import { Menu, MenuProps } from 'antd';
import { MainSidebarWrapper, MenuCustoms } from './MainSidebarStyled';
// import MenuGroup from './custom-menu/menu-group';
// import { menuCMS } from '../../../../utils/menu-json';
import { useRouter } from 'next/router';
import { LogoWebWrapper } from '../MainLayoutStyled';
import Image from 'next/image';
import LogoWeb from '@/assets/images/logo_admin.png';
import { UploadOutlined, UserOutlined, VideoCameraOutlined } from '@ant-design/icons';
import { menuCMS } from './menu-json';

type MenuItem = Required<MenuProps>['items'][number];

interface IMainSidebarProps {}
interface IMenu {
  name?: string;
  label: string;
  icon?: any;
  path: string;
  roles?: string;
  childrens?: any;
}
function getItem(
  label: React.ReactNode,
  key: React.Key,
  icon?: React.ReactNode,
  children?: MenuItem[],
  type?: 'group'
): MenuItem {
  return {
    key,
    icon,
    children,
    label,
    type,
  } as MenuItem;
}

const MainSidebar = (props: IMainSidebarProps) => {
  const [collapsed, setCollapsed] = useState<boolean>(false);
  const [menuKey, setMenuKey] = useState<any>('my-files');

  const router = useRouter();

  const items2: MenuProps['items'] = menuCMS.map((item, index) => {
    return {
      key: item.path,
      icon:  <item.icon className="test" />,
      label: item.label,
      children:
        item.childrens.length > 0
          ? item.childrens.map((record) => {
              return {
                key: record.path,
                label: record.label,
              };
            })
          : null,
    };
  });
  // const renderMenu = (item: any) => {
  //   return (
  //     item &&
  //     item.map((item: IMenu) => {
  //       if (item.childrens && item.childrens.length > 0) {
  //         return getItem(
  //           item.label,
  //           item.path,
  //           <item.icon stroke={'white'} fill="red" className="test" />,
  //           [...renderMenu(item.childrens)]
  //         );
  //       } else {
  //         if (item.icon) {
  //           return getItem(
  //             item.label,
  //             item.path,
  //             <item.icon
  //               //  stroke={'red'} strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round"
  //               className="test"
  //             />
  //           );
  //         } else {
  //           return getItem(item.label, item.path, null);
  //         }
  //       }
  //     })
  //   );
  // };

  const handleNavigate = (menu: any) => {
    if (menu.key === '/') {
      router.push(`/`);
      setMenuKey(menu.keyPath);
    } else {
      setMenuKey(menu.keyPath);
      if (menu.keyPath.length) {
        router.push(`/${menu.keyPath.reverse().join('/')}`);
      } else {
        router.push(`/${menu.key}`);
      }
    }
  };
  useEffect(() => {
    const segment: any = router.pathname.split('/').filter((item) => item !== '');
    setMenuKey(segment.reverse() || '/');
  }, [router.pathname]);
  return (
    <MainSidebarWrapper
      collapsed={collapsed}
      onCollapse={(value) => setCollapsed(value)}
      breakpoint="lg"
      width={230}
      collapsedWidth="0"
      onBreakpoint={(broken) => {}}
    >
      <MenuCustoms
        theme="light"
        mode="inline"
        onClick={handleNavigate}
        items={items2}
        selectedKeys={menuKey}
        // inlineCollapsed={collapsed}
      />
    </MainSidebarWrapper>
  );
};
export default MainSidebar;
