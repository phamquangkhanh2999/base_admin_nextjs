import Icon_Home from '@/assets/icons-menu/home.svg';
import Icon_School from '@/assets/icons-menu/location.svg';
import Icon_Student from '@/assets/icons-menu/ph_student-bold.svg';
import Icon_Parents from '@/assets/icons-menu/two_people.svg';
import Icon_Hr_Record from '@/assets/icons-menu/profile.svg';
import Icon_Tuition_Fee from '@/assets/icons-menu/cash_money.svg';
import Icon_Hotel from '@/assets/icons-menu/hotel.svg';
import Icon_Actions_Plus from '@/assets/icons-menu/actions_plus.svg';
import Icon_Calandar from '@/assets/icons-menu/calandar.svg';
import Icon_Paste from '@/assets/icons-menu/paste.svg';
import Icon_Settings from '@/assets/icons-menu/settings.svg';
export const menuCMS = [
  {
    name: 'home-page',
    label: 'Trang chủ',
    icon: Icon_Home,
    path: 'home-page',
    roles: '',
    childrens: [],
  },
  {
    name: 'school-information',
    label: 'Thông tin trường học',
    icon: Icon_School,
    path: 'school-information',
    roles: '',
    childrens: [],
  },
  {
    name: 'student',
    label: 'Học sinh',
    icon: Icon_Student,
    path: 'student',
    roles: '',
    childrens: [],
  },
  {
    name: 'parents',
    label: 'Phụ huynh',
    icon: Icon_Parents,
    path: 'parents',
    roles: '',
    childrens: [],
  },
  {
    name: 'hr-record',
    label: 'Hồ sơ nhân sự',
    icon: Icon_Hr_Record,
    path: 'hr-record',
    roles: '',
    childrens: [],
  },
  {
    name: 'tuition-fee',
    label: 'Học phí',
    icon: Icon_Tuition_Fee,
    path: 'tuition-fee',
    roles: '',
    childrens: [
      {
        name: 'school-wide-tuition',
        label: 'Học phí toàn trường',
        path: 'school-wide-tuition',
        roles: '',
      },
      {
        name: 'main-fee-collection',
        label: 'Thu phí chính khóa',
        path: 'main-fee-collection',
        roles: '',
      },
      {
        name: 'extra-curricular-fees',
        label: 'Thu phí ngoại khóa',
        path: 'extra-curricular-fees',
        roles: '',
      },
    ],
  },
  {
    name: 'class-room',
    label: 'Lớp học',
    icon: Icon_Hotel,
    path: 'class-room',
    roles: '',
    childrens: [],
  },
  {
    name: 'learn-more',
    label: 'Coi thêm',
    icon: Icon_Actions_Plus,
    path: 'learn-more',
    roles: '',
    childrens: [],
  },
  {
    name: 'events',
    label: 'Sự kiện',
    icon: Icon_Calandar,
    path: 'events',
    roles: '',
    childrens: [],
  },
  {
    name: 'course',
    label: 'Khóa học',
    icon: Icon_Paste,
    path: 'course',
    roles: '',
    childrens: [],
  },
  {
    name: 'system-configuration',
    label: 'Cấu hình hệ thống',
    icon: Icon_Settings,
    path: 'system-configuration',
    roles: '',
    childrens: [],
  },
];
