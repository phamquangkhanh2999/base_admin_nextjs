import styled from 'styled-components';
import { Layout, Dropdown, Menu } from 'antd';
const { Sider } = Layout;

export const MainSidebarWrapper = styled(Sider)`
  transition: all 0.2s !important;
  position: fixed !important;
  height: 94vh !important;
  top: 64px !important;
  z-index: 99;
  .ant-layout-sider-children {
    background: ${({ theme }) => theme.colors.colorBlue400} !important;
    /* box-shadow: 0px 2px 8px rgba(0, 0, 0, 0.15) !important; */
    /* box-shadow: 0px 2px 2px rgba(163, 171, 185, 0.26); */
    /* /* margin-top: 55px; */
    padding-top: 12px;
  }
  .ant-layout-sider-zero-width-trigger {
    top: 0px;
  }
`;

export const MenuCustoms = styled(Menu)`
  background: #e2f1f9 !important;

  .ant-menu-item {
    color: #000000;
    font-family: 'Roboto';
    font-style: normal;
    font-weight: 400;
    font-size: 14px;
    line-height: 160%;
    /* padding-left: 10px !important; */
  }
  .ant-menu-item-selected,
  .ant-menu-item-active {
    background: #4079a3 !important;
    color: #ffffff !important;
    border-radius: 8px !important;
    svg {
      filter: invert(63%) sepia(136%) saturate(1394%) hue-rotate(200deg) brightness(158%);
    }
  }
  :where(.css-dev-only-do-not-override-ixblex).ant-menu-light
    .ant-menu-submenu-selected
    > .ant-menu-submenu-title {
    font-weight: 600;
    color: #000000;
  }
`;
