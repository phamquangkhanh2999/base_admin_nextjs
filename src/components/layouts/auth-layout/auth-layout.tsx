import { LayoutProps } from '../../../models/common';
import { CaretDownOutlined, GlobalOutlined } from '@ant-design/icons';
import React, { useEffect, useRef, useState } from 'react';
import {
  LanguageWrapper,
  LayoutAuthCenter,
  LayoutAuthFooter,
  LayoutAuthTop,
  LayoutWrapper,
  LogoAuth,
  LayoutAuthOrganization,
} from './AuthLayoutStyled';
import { Avatar, Select } from 'antd';
const { Option } = Select;

import { useRouter } from 'next/router';
// import { BoxUserMenuBussiness, ButtonInfomation, ButtonLogOut, MenuAvatarText, MenuAvatarWrapper, UserMenu, WrappBoxAvatar } from '../../common/header/MainHeaderStyled';

const AuthLayout = ({ children }: LayoutProps) => {
  const [localization2, setLocalization] = useState<any>();
  const storage = typeof window !== 'undefined' ? localStorage.getItem('lenguage') : undefined;
  const wrapperRef = useRef(null);
  const router = useRouter();
  const [openUserMenu, setOpenUserMenu] = useState<boolean>(false);

  const [data, setData] = useState<any>();

  const auth = false;

  const handleOpenUserMenu = () => {
    setOpenUserMenu(!openUserMenu);
  };

  const useOutsideAlerter = (ref: any) => {
    useEffect(() => {
      function handleClickOutside(event: any) {
        if (ref.current && !ref.current.contains(event.target)) {
          setOpenUserMenu(false);
        }
      }
      document.addEventListener('mousedown', handleClickOutside);
      return () => {
        document.removeEventListener('mousedown', handleClickOutside);
      };
    }, [ref]);
  };

  useOutsideAlerter(wrapperRef);

  return (
    <LayoutWrapper>
      <LayoutAuthTop>
        <LanguageWrapper>
          {/* <MenuAvatarWrapper
            onClick={handleOpenUserMenu}
            style={{ display: 'flex', alignItems: 'center' }}
          >
            <UserMenu>
              <Avatar
                size={24}
                // icon={<UserOutlined />}
                style={{ marginRight: '10px' }}
                src={data?.Avatar?.AbsoluteUri}
              />
              <MenuAvatarText style={{ marginRight: '10px', color: 'black' }}>
                {data?.DisplayName}
              </MenuAvatarText>
            </UserMenu>
          </MenuAvatarWrapper> */}
        </LanguageWrapper>

        {/* {openUserMenu ? (
          <BoxUserMenuBussiness style={{ right: '8%' }} ref={wrapperRef}>
            <WrappBoxAvatar>
              <Avatar
                size={120}
                // icon={<UserOutlined />}
                style={{ marginRight: '10px' }}
                src={data?.Avatar?.AbsoluteUri}
              />
            </WrappBoxAvatar>
            <div className="name">{data?.DisplayName}</div>
            <div
              style={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                marginBottom: '12px',
              }}
            >
              <ButtonInfomation
                onClick={() => router.push(`/employee-management/employees/${data?.Id}`)}
              >
                Thông tin tài khoản
              </ButtonInfomation>
            </div>
            <div
              style={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                marginTop: '12px',
              }}
            >
              <ButtonLogOut>Đăng xuất</ButtonLogOut>
            </div>
          </BoxUserMenuBussiness>
        ) : null} */}
        {auth ? <LogoAuth></LogoAuth> : null}
        {auth ? (
          <LayoutAuthCenter>{children}</LayoutAuthCenter>
        ) : (
          <LayoutAuthOrganization>{children}</LayoutAuthOrganization>
        )}
      </LayoutAuthTop>
    </LayoutWrapper>
  );
};

export default AuthLayout;
