export type Colors = {
  backgroundGlobals?: string;
  colorText?: string;
  colorButtonDefault?: string;
  colorButtonActive?: string;
  backgroundButton?: string;
  colorBtnBorderDefault?: string;
  colorBtnBorderActive?: string;
  colorBorderCard?: string;
  backgroundCard?: string;
  colorBlue700?: string;
  colorBlue400?: string;
  colorTextTitle?: string;
  backgroundHeadTable?: string;
};

export type MediaQueries = {
  mobile_xs?: string;
  mobile?: string;
  tablet?: string;
  laptop?: string;
  pc?: string;
};

export type Shadows = {};
export type Borders = {};
