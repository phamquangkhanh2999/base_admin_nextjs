import { Colors } from './types-theme';

export const baseColors: Colors = {
  colorBlue700: '#4079A3',
  colorBlue400: '#E2F1F9',
  colorTextTitle: '#0F2D59',
  colorText: '#333333',
  colorButtonDefault: '#BB2649',
  colorButtonActive: '#BB2649',
  backgroundButton: '#BB2649',
  colorBtnBorderDefault: '#fe7411',
  colorBtnBorderActive: '#BB2649',
  colorBorderCard: '#bb26494f',
  backgroundCard: '#FFFFFF',
  backgroundHeadTable: '#E2F1F9',
};
