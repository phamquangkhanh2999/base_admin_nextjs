import { Borders, MediaQueries, Shadows } from './types-theme';

export const breakpointMap: { [key: string]: number } = {
  xs: 0,
  sm: 576,
  md: 768,
  lg: 992,
  xl: 1200,
  xxl: 1600,
};

const mediaQueries: MediaQueries = {
  mobile: `@media only screen and (min-width: ${breakpointMap.s}px) and (max-width:${breakpointMap.sx}px)`,
  tablet: `@media only screen and (min-width: ${breakpointMap.md}px) and (max-width:${breakpointMap.lg}px)`,
};

// const mediaQueries: MediaQueries = {
//   mobile_xs: `@media only screen and (min-width: ${breakpointMap.s}px) and (max-width:480px)`,
//   mobile: `@media only screen and (min-width: ${breakpointMap.s}px) and (max-width:${breakpointMap.sx}px)`,
//   tablet: `@media only screen and (min-width: ${breakpointMap.md}px) and (max-width:${breakpointMap.lg}px)`,
//   laptop: `@media screen and (min-width: ${breakpointMap.xl}px) and (max-width:${breakpointMap.xl_1}px)`,
//   pc: `@media screen and (min-width: ${breakpointMap.xxl}px)`,
// };

export const shadows: Shadows = {};

export const borders: Borders = {
  borderDefault: '1px solid #BB2649',
};

export const borderRadius: Borders = {
  radiusDefault: '8px',
};

const myObject = {
  mediaQueries,
  shadows,
  borders,
  borderRadius,
};

export default myObject;

// ${({ theme }) => theme.mediaQueries.mobile} {
// }
// // tablet
// ${({ theme }) => theme.mediaQueries.tablet} {
// }
// // laptop
// ${({ theme }) => theme.mediaQueries.laptop} {
// }
