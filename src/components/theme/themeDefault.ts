import { DefaultTheme } from 'styled-components';
import base from './base';
import { baseColors } from './colors';

const themeDefault: DefaultTheme = {
  ...base,
  colors: baseColors,
};

export default themeDefault;
