FROM node:16-alpine as builder
LABEL maintainer="quangkhanh2999@gmail.com"
LABEL description="This is the build stage for app. Here we generate static resource."

# App directory
WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
# App dependencies
COPY package.json yarn.lock* package-lock.json* pnpm-lock.yaml* ./
RUN \
  if [ -f yarn.lock ]; then yarn --frozen-lockfile; \
  elif [ -f package-lock.json ]; then npm ci; \
  elif [ -f pnpm-lock.yaml ]; then yarn global add pnpm && pnpm i --frozen-lockfile; \
  else echo "Lockfile not found." && exit 1; \
  fi


# Copy app source code & env
COPY . ./

# Add bash
RUN apk add --no-cache

# Định nghĩa biến môi trường
ARG VITE_API_URL
ENV VITE_API_URL=$VITE_API_URL
# Generate static
RUN yarn build

# ===== SECOND STAGE ======
FROM nginx:alpine
LABEL maintainer="quangkhanh2999@gmail.com"
LABEL description="This is the 2nd stage: a very small image where we copy the static resource."

# App directory
WORKDIR /app

# Setup nginx serve in SPA mode
RUN echo $'server {\n\
	listen 3000 default_server;\n\
	gzip on;\n\
	gzip_min_length 1000;\n\
	gzip_types text/plain text/xml application/javascript text/css;\n\
	root /app;\n\
	location / {\n\
		try_files $uri $uri/index.html /index.html;\n\
	}\n\
	location ~ \.(?!html) {\n\
		add_header Cache-Control "public, max-age=2678400";\n\
		try_files $uri =404;\n\
	}\n\
}' > /etc/nginx/conf.d/default.conf

# Copy static resource
# COPY --from=builder /app/build/ ./

# Default port exposure
EXPOSE 3000

# Start server
CMD ["nginx", "-g", "daemon off;"]