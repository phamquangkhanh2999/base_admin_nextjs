/** @type {import('next').NextConfig} */
const path = require('path');
const nextConfig = {
  reactStrictMode: true,
  compiler: {
    styledComponents: true,
  },
  sassOptions: {
    includePaths: [path.join(__dirname, 'styles')],
  },
  env: {
    BASE_URL: process.env.BASE_URL,
    BASE_UPLOAD_URL: process.env.BASE_UPLOAD_URL,
  },
  typescript: {
    ignoreBuildErrors: true,
  },
  // experimental: {
  //   outputStandalone: true,
  // },

  // optimizeFonts: false,

  // presets: ['next/babel'],
  webpack(config) {
    config.module.rules.push({
      test: /\.svg$/,
      use: [{loader: '@svgr/webpack', options: {icon: true}}],
    })
    return config;
  },
};

module.exports = nextConfig;
