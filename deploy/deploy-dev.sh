#!/bin/bash

VERSION_FILE="deploy/version.txt"
LOG_FILE="deploy/version_changes.log"

# Kiểm tra xem tệp tin phiên bản có tồn tại hay không
if [ ! -f "$VERSION_FILE" ]; then
    echo "1.0" > "$VERSION_FILE" # Nếu không tồn tại, tạo file và ghi phiên bản mặc định là 1.0
fi

# Đọc phiên bản hiện tại từ tệp tin
current_version=$(cat "$VERSION_FILE")

# Lưu phiên bản hiện tại vào biến old_version
old_version="$current_version"

# Tăng giá trị phiên bản (ví dụ: tăng minor version lên 1)
IFS='.' read -r major minor <<< "$current_version"
((minor++))

# Cập nhật giá trị phiên bản mới
tag="$major.$minor"
echo "$tag" > "$VERSION_FILE"

# Lưu phiên bản cũ và phiên bản mới vào file log
echo "Version changed from $old_version to $tag" >> "$LOG_FILE"

baseUrl="http://103.82.26.253:8000";
DOCKER_USERNAME="codechub"
DOCKER_PASSWORD="123@45678"
IMAGE_NAME="sso-rar-cms-fe";

#check image hiện tại và xoá đi 
if docker images "$IMAGE_NAME:$old_version" | grep -q "$IMAGE_NAME"; then
    echo "Image $IMAGE_NAME:$old_version tồn tại. Đang xóa..."
    docker rmi "$IMAGE_NAME:$old_version"
    echo "Đã xóa image $IMAGE_NAME:$old_version thành công."
else
    echo "Không tìm thấy image $IMAGE_NAME:$old_version."
fi

#check image hiện tại và xoá đi 
if docker images "$DOCKER_USERNAME/$IMAGE_NAME:$old_version" | grep -q "$DOCKER_USERNAME/$IMAGE_NAME"; then
    echo "Image $DOCKER_USERNAME/$IMAGE_NAME:$old_version tồn tại. Đang xóa..."
    docker rmi "$DOCKER_USERNAME/$IMAGE_NAME:$old_version"
    echo "Đã xóa image $DOCKER_USERNAME/$IMAGE_NAME:$old_version thành công."
else
    echo "Không tìm thấy image $DOCKER_USERNAME/$IMAGE_NAME:$old_version."
fi


echo  "Tags: $tag";
echo "==============START BUILD DOCKER=============="
docker build --build-arg API_URL=$baseUrl -t "$IMAGE_NAME:$tag" -f deploy/Dockerfile-dev .
echo "BUILD DOCKER SUCCESS";
sleep 10

echo "Auto login to $DOCKER_USERNAME"
docker login --username=$DOCKER_USERNAME --password=$DOCKER_PASSWORD

# echo "login manual...";
# echo "login to $DOCKER_USERNAME"
# echo "password:"
# read pass
# echo "$pass" | docker login --username=$DOCKER_USERNAME --password-stdin

echo "Start push dockerhub";
docker tag "$IMAGE_NAME:$tag" "$DOCKER_USERNAME/$IMAGE_NAME:$tag"
sleep 10
docker push  "$DOCKER_USERNAME/$IMAGE_NAME:$tag"
echo "Push dockerhub successfully!";
sleep 10
docker image prune -f
echo "prune image!";

sleep 30
